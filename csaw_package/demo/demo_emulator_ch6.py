# -*- encoding: utf-8 -*-

from unicorn.arm_const import *
from csawesc.emulator import TeensyEmulator

VALUE = 0
DONE = 0

def custom_hook_code(mu, address, size, user_data):
    """A custom hook code
    Bruteforce a byte to find the one producing a given hash.
    """
    # DEBUG
    #print("(debug) Tracing instruction at {} (size = {})".format(hex(address), size))

    # Replace the byte read from the RFID card packet.
    # Offset = 0x1B24.
    global VALUE
    if address == 0x1B24:
        # Set R3
        mu.reg_write(UC_ARM_REG_R3, VALUE)
        VALUE += 1

    #  Get the computed hash before the strmcp().
    global DONE
    if address == 0x1B70:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        input_hash = mu.mem_read(r3, 32)
        print("R3 = {}. Hash = {}".format(hex(r3), input_hash))
        if input_hash == bytearray(b'242b461d0b97cca55e5d62372b770ab4'):
            print('[+] Value = {}'.format(hex(VALUE-1)))
            DONE = 1


def solve():
    """CSAW'19 ESC Challenge n°X
    Open the door <name>
    """

    emulator = TeensyEmulator(firmware="/home/bromain/Documents/csaw-esc/challenge-releases/ChallengeSetB/TeensyChallengeSetB.ino.elf", debug=1)
    # emulator = TeensyEmulator(firmware="/home/math/csaw/final/ChallengeSetB/TeensyChallengeSetB.ino.elf", debug=1)

    # Define instruction range to emulate
    emulator.fct_start = 0x1AF8
    emulator.fct_end = 0x1B72

    # Replace the default hook code method
    # We can do the same for the hook invalid mem method
    emulator.hook_code = custom_hook_code

    # Start
    for i in range(256):
        if DONE:
            break
        emulator.emulate()


if __name__ == '__main__':
    solve()
