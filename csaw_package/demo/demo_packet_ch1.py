# -*- encoding: utf-8 -*-

from csawesc.packet import Packet

chall_hash = [0x0]*32
chall_ready = False

def solve():
    """CSAW'19 ESC Challenge n°1
    Open the door closet
    """
    # Known hardcoded byte array using Ghidra
    hardcoded_string = list(bytes('ESC19-rocks!', 'utf-8'))

    # Instantiate a new packet object
    p = Packet()

    # Put values in the tag
    print('[-] Resolving the first 12 values...')
    p.RFID[92:104] = [i+12 for i in range(0, 12)]

    p.RFID[104:112] = hardcoded_string[0:8]
    p.RFID[128:132] = hardcoded_string[8:12]

    # Submit our packet to the revserse function to test our values
    valid = reverse(p)

    if valid:
        # Generate the challenge validation hash
        print('[!] Validation hash: {}'.format(p.compute_hash()))
        # Generate the sender.py
        p.export_sender(path='.', name='sender1.py')
        # Transmit the RFID values throught UART to the Teensy inorder to program the tag
        # p.send_serial(comm=b'p')


def reverse(p):
    """RE of the 'challenge_1' function from 'TeensyChallengeSet1.ino.elf' firmware.
    :p: The submitted packet
    :return (bool): True if valid, False if invalid
    """
    read_values = [0x0]*132
    hardcoded_string = list(b'ESC19-rocks!')
        
    print('[-] Loop 1 : Reading 24 bytes from RFID [92..111] and [128..131]:')
    for i in range(92, 132):
        if (i < 112):
            read_values[i+16] = p.RFID[i]
        elif (i > 127):
            read_values[i] =  p.RFID[i]

    print('[-] Verifying read values...')
    is_valid = True
    for i in range(0, 12):
        if(hardcoded_string[i] != read_values[read_values[i+108]+108]):
            is_valid = False
    
    read_values[72:104] = list(b'solved challenge closet abcdefg ')

    if (is_valid == True):
        print('[!] Success')
        for i in range(0, 31):
            chall_hash[i] = read_values[i+72]
    else:
        print('[!] Fail')
        for i in range(0, 31):
            chall_hash[i] = ord('\xff')

    chall_hash[31] = ord('\x01')

    print('[!] Flag :', bytes(chall_hash))

    return is_valid


if __name__ == '__main__':
    solve()