#!/usr/bin/env python3

import os

from unicorn import *
from unicorn.arm_const import *

BASE = 0
# Start of section .text
TEXT = 0x10000
STACK_ADDR = 0x0
STACK_SIZE = 1024*1024
PAGE_SIZE = 0x1000

# Information related to the function to emulate
FCT_START = 0x0
# Not the actual end, we don't need to execute everything
FCT_END = 0x0
VALUE = 0
DONE = 0

REGS = [ UC_ARM_REG_R0, UC_ARM_REG_R1, UC_ARM_REG_R2, UC_ARM_REG_R3,
         UC_ARM_REG_R4, UC_ARM_REG_R5, UC_ARM_REG_R6, UC_ARM_REG_R7,
         UC_ARM_REG_R8, UC_ARM_REG_R9, UC_ARM_REG_R10, UC_ARM_REG_R11,
         UC_ARM_REG_R12, UC_ARM_REG_PC, UC_ARM_REG_SP, UC_ARM_REG_LR,
         UC_ARM_REG_CPSR ]

def ALIGN_PAGE_DOWN(x):
    return x & ~(PAGE_SIZE - 1)

def ALIGN_PAGE_UP(x):
    return (x + PAGE_SIZE - 1) & ~(PAGE_SIZE-1)

def hook_mem_invalid(emu, access, addr, size, value, user_data):
    """
    Catch illegal memory operation. Allocate desired memory and continue.
    """
    if access == UC_MEM_WRITE_UNMAPPED:
        print("[-] Trying to write at {}".format(hex(addr)))
        # Trying to write to unmapped memory. Allocating some at target address
        # before resuming.
        s = ALIGN_PAGE_DOWN(addr)
        e = ALIGN_PAGE_UP(addr+size)
        emu.mem_map(s, e - s)

        # Return True to indicate we want to continue emulation
        return True
    elif access == UC_MEM_READ_UNMAPPED:
        print("[-] Trying to read at {}".format(hex(addr)))
        s = ALIGN_PAGE_DOWN(addr)
        e = ALIGN_PAGE_UP(addr+size)
        emu.mem_map(s, e - s)

        # There is a byte read at an unmapped memory address which is used to
        # pad the hash. This byte is 0x80.
        if addr == 0x1fff8804:
            emu.mem_write(addr, b'\x80')

        return True

def hook_code(mu, address, size, user_data):
    # DEBUG OUTPUT
    #print("(debug) Tracing instruction at {} (size = {})".format(hex(address), size))

    # Read a register
    #global DONE
    if address == 0x0:
        r2 = mu.reg_read(UC_ARM_REG_R2)
        print("R2 = {}".format(r2))
        #if r2 == X:
        #    DONE = 1

    # Write into a register
    if address == 0x0:
        mu.reg_write(UC_ARM_REG_R3, VALUE)

    # Write in memory
    if address == 0x0:
        mu.mem_write(0x1792, b'\x02\x01')

def read_data(filename):
    with open(filename, "rb") as f:
        # Copy only from TEXT to synchronize instruction addresses with the
        # disassembly since .text section as a decalage of TEXT.
        f.seek(TEXT, 0)
        data = f.read()
        f.close()
    return data

def emulate(filename, size):
    print("[+] Setting up emulation environment..")
    mu = Uc(UC_ARCH_ARM, UC_MODE_THUMB)

    # Map the binary
    mu.mem_map(BASE, ALIGN_PAGE_UP(size))
    mu.mem_write(BASE, read_data(filename))
    print("[+] Binary mapped.")

    # Map the stack
    global STACK_ADDR
    STACK_ADDR = ALIGN_PAGE_UP(BASE + size)
    mu.mem_map(STACK_ADDR, STACK_SIZE)
    print("[+] Stack mapped.")

    # Set registers
    for r in REGS:
        mu.reg_write(r, 0)
    # -1024 so a function fetching arguments will not access unmapped memory.
    mu.reg_write(UC_ARM_REG_SP, STACK_ADDR + STACK_SIZE-1024)
    print("[+] Registers set.")

    # Register hooks and start emulation
    mu.hook_add(UC_HOOK_MEM_READ_UNMAPPED | UC_HOOK_MEM_WRITE_UNMAPPED, hook_mem_invalid)
    mu.hook_add(UC_HOOK_CODE, hook_code)
    print("[+] All set. Now running..")

    # Note we start at ADDRESS | 1 to indicate THUMB mode.
    mu.emu_start(FCT_START | 1, FCT_END)

if __name__ == "__main__":

    filename = ""
    size = os.path.getsize(filename)

    #for i in range(256):
    #    print("\n[-] Trying VALUE = {}".format(VALUE))
    #    emulate(filename, size)
    #    VALUE += 1
    #    if DONE == 1:
    #        print("Done!")
    #        break

    emulate(filename, size)
