# -*- coding: utf-8 -*-

import setuptools


with open('README.md', 'r') as fh:
    long_description = fh.read()


setuptools.setup(
    name='csawesc',
    version='0.0.1',
    author='UBS BZH',
    description='A small package containing tools to resolve CSAW19 ESC challenges',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent'
    ],
    install_requires=[
        'pyserial == 3.2.1',
        'cryptography >= 2.8',
        'unicorn >= 1.0.1'
    ],
    python_requires='>=3.7'
)