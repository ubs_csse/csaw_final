# Python Package 'csawesc'

## Overview

This Python package aims to enhance our analysis and scripting efficiency for the live challenge.
This package gather all our reverse engineering techniques :
- A packet class reproduces reproduces the usage of the packet structure in the board.
- For our dynamic analysis, we used Unicorn Engine. To simplify our original template to emulate a target function of the Teensy, we developed a dedicated class.  

## Usage

The usage of this package is explained througth two demos located in `demo/`.

