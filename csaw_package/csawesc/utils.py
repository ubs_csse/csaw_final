# -*- coding: utf-8 -*-

import struct


def bytes2uint(byte_list, endianess='<'):
    """Return a list of bytes into an unsigned int
    byte_list: list(bytes())
    endianess: '<' little endian, '>' big endian
    return: unsigned int
    """
    return struct.unpack(endianess+'I', bytes(byte_list))[0]


def uint2bytes(uint, endianess='<'):
    """Return an usigne dint to a list of bytes
    uint: unsigned int
    endianess: '<' little endian, '>' big endian
    return: list(bytes())
    """
    return list(struct.pack(endianess+'I', uint))