# -*- coding: utf-8 -*-

import serial
import hashlib


class Packet(object):
    """Describe the packet structure used by the Teensy and the AVR.
    :comm (byte): The communication packet identifier.
    :RFID (int[1024]): The values that will be loaded in the RFID tag.
    :keys (int[48]): The keys array (unknown usage)
    :buttons (int): The value of the buttons (A & B).
    :challenge_num (int): The challenge number.
    """

    def __init__(self, comm=b'', RFID=[0x0]*1024, keys=[0X0]*48, buttons=0x0, challenge_num=0x0):
        self._comm = comm
        self._RFID = RFID
        self._keys = keys
        self._buttons = buttons
        self._challenge_num = challenge_num

    def send_serial(self, comm=b'p'):
        """Send over serial the packet to the Teensy in order to program the RFID tag.
        :comm (byte): b'p' only the RFID will be loaded (enables 'packetAvailable' to true).
                      b'a' only the keys will be loaded (enables 'authAvailable' to true).
                      b'c' only the RFID, buttons, challenge_num will be loaded.
        :return: None
        """
        p = [[self._RFID[j] for j in range(i, i+16)] for i in range(0, 1024, 16)]
        k = [[self._keys[j] for j in range(i, i+3)] for i in range(0, 48, 3)]

        ser = serial.Serial('/dev/ttyACM0', 2000000)

        ser.write(comm)

        for i in range(64):
            ser.write(p[i])

        for i in range(16):
            ser.write(k[i])

        ser.write([self._buttons, self._challenge_num])        

    def compute_hash(self):
        """Compute the hash from the RFID, a and b entries.
        The hash is used to valid a challenge.
        :return (str): hexdigest 
        """
        p = [[self._RFID[j] for j in range(i, i+16)] for i in range(0, 1024, 16)]

        entry = (''.join([''.join(str(x)) for x in p]))
        entry += str(self._buttons >> 4)
        entry += str(self._buttons & 0xf)

        return hashlib.sha256(entry.encode('utf-8')).hexdigest()

    def export_sender(self, path, name='sender.py'):
        """Export a sender.py like file in order to program the RFID tag.
        :path (str): The path where to locate the sender.py
        :name (str): The name of the file to export.
        """
        start = """
        import serial
        import hashlib

        ser = serial.Serial('/dev/ttyACM0', 2000000)
        """.replace("    ", "")

        arrays = """
        p = [
            {}]

        k = [
            {}]
        """.replace("    ", "").format(self._rfid_to_str(), self._keys_to_str())

        end = """
        a = {}
        b = {}

        entry = (''.join([''.join(str(x)) for x in p]))
        entry += str(a)
        entry += str(b)

        print(hashlib.sha256(entry.encode('utf-8')).hexdigest())

        ser.write(b'p')

        for i in range(64):
        \tser.write(p[i])

        for i in range(16):
        \tser.write(k[i])

        ser.write(b'\\xaa\\xbb') 
        """.replace("    ", "").format(hex(self._buttons >> 4).replace("'", ""), hex(self._buttons & 0xf).replace("'", ""))

        with open(path+'/'+name, 'w') as file:
            file.write(start)
            file.write(arrays)
            file.write(end)

    def clear_packet(self):
        """Clear the packet.
        """
        self._RFID = [0x0]*1024
        self._keys = [0x0]*48

    def debug_print_packet(self):
        """Pretty display of the entire packet.
        """
        print(self._rfid_to_str())
        print(self._keys_to_str())
        print('\t', hex(self._buttons), '|', self._buttons)
        print('\t', hex(self._challenge_num), '|', self._challenge_num)

    def display_rfid(self):
        """Pretty display of the RFID content.
        """
        print(self._rfid_to_str())

    def display_keys(self):
        """Pretty display of the keys content.
        """
        print(self._keys_to_str())

    def _rfid_to_str(self):
        """Convert the RFID content to string with chunk & offsets informations.
        TODO: Add sectors
        """
        res = ''
        rfid = [[hex(self._RFID[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

        imin, imax = (0, 15) 
        for i in range(len(rfid)):
            row = '{}, # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", "")
            res += '\t' + row + '\n'
            imin = imax+1
            imax += 16
        
        return res

    def _keys_to_str(self):
        """Convert the keys content to string with offsets informations.
        """
        res = ''
        keys = [[hex(self._keys[j]) for j in range(i, i+3)] for i in range(0, 48, 3)]
        imin, imax = (0, 3) 

        for i in range(len(keys)):
            row = '{}, # {} [{}..{}]'.format(keys[i], i, imin, imax).replace("'", "")
            res += '\t' + row + '\n'
            imin = imax+1
            imax += 3

        return res

    @property
    def comm(self):
        return self._comm

    @comm.setter
    def comm(self, v):
        self._comm = v

    @property
    def RFID(self):
        return self._RFID

    @RFID.setter
    def RFID(self, v):
        self._RFID = v

    @property
    def keys(self):
        return self._keys

    @keys.setter
    def keys(self, v):
        self._keys = v

    @property
    def buttons(self):
        return self._buttons

    @buttons.setter
    def buttons(self, v):
        self._buttons = v

    @property
    def challenge_num(self):
        return self._challenge_num

    @challenge_num.setter
    def challenge_num(self, v):
        self._challenge_num = v
