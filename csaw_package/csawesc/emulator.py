# -*- coding: utf-8 -*-

from unicorn import *
from unicorn.arm_const import *

import os


class TeensyEmulator():
    """Emulator for the Teensy 3.2 
    """

    def __init__(self, firmware, debug=1):
        self.base = 0
        # Start of section .text
        self.text = 0X10000
        self.stack_addr = 0x0
        self.stack_size = 1024*1024
        self.page_size = 0x1000

        # Information related to the function to emulate
        self.fct_start = 0x0
        # Not the actual end, we don't need to execute everything
        self.fct_end = 0x0
        #self.value = 0
        #self.done = 0

        self.regs = [
            UC_ARM_REG_R0, UC_ARM_REG_R1, UC_ARM_REG_R2, UC_ARM_REG_R3,
            UC_ARM_REG_R4, UC_ARM_REG_R5, UC_ARM_REG_R6, UC_ARM_REG_R7,
            UC_ARM_REG_R8, UC_ARM_REG_R9, UC_ARM_REG_R10, UC_ARM_REG_R11,
            UC_ARM_REG_R12, UC_ARM_REG_PC, UC_ARM_REG_SP, UC_ARM_REG_LR,
            UC_ARM_REG_CPSR
        ]

        self._debug = debug

        self._firmware = firmware
        self._size = os.path.getsize(firmware)
        self._data = None
        # Read firmware data
        with open(firmware, "rb") as f:
            # Copy only from self.text to synchronize instruction addresses with the
            # disassembly since .text section as a decalage of self.text.
            f.seek(self.text, 0)
            self._data = f.read()
            f.close()
    
    def emulate(self):
        """
        """
        print("[+] Setting up emulation environment..")
        mu = Uc(UC_ARCH_ARM, UC_MODE_THUMB)

        # Map the binary
        mu.mem_map(self.base, self._align_page_up(self._size))
        mu.mem_write(self.base, self._data)
        print("[+] Binary mapped.")

        # Map the stack
        self.stack_addr = self._align_page_up(self.base + self._size)
        mu.mem_map(self.stack_addr, self.stack_size)
        print("[+] Stack mapped.")

        # Set registers
        for r in self.regs:
            mu.reg_write(r, 0)
        # -1024 so a function fetching arguments will not access unmapped memory.
        mu.reg_write(UC_ARM_REG_SP, self.stack_addr + self.stack_size - 1024)
        print("[+] Registers set.")

        # Register hooks and start emulation
        mu.hook_add(UC_HOOK_MEM_READ_UNMAPPED | UC_HOOK_MEM_WRITE_UNMAPPED, self.hook_mem_invalid)
        mu.hook_add(UC_HOOK_CODE, self.hook_code)
        print("[+] All set. Now running...")

        # Note we start at ADDRESS | 1 to indicate THUMB mode.
        mu.emu_start(self.fct_start | 1, self.fct_end)

    def hook_mem_invalid(self, emu, access, addr, size, value, user_data):
        """Catch illegal memory operation. Allocate desired memory and continue.

        :return (bool):
        """
        if access == UC_MEM_WRITE_UNMAPPED:
            print("[-] Trying to write at {}".format(hex(addr)))
            # Trying to write to unmapped memory. Allocating some at target address
            # before resuming.
            s = self._align_page_down(addr)
            e = self._align_page_up(addr+size)
            emu.mem_map(s, e-s)

            # Return True to indicate we want to continue emulation
            return True
        elif access == UC_MEM_READ_UNMAPPED:
            print("[-] Trying to read at {}".format(hex(addr)))
            s = self._align_page_down(addr)
            e = self._align_page_up(addr+size)
            emu.mem_map(s, e - s)

            # There is a byte read at an unmapped memory address which is used to
            # pad the hash. This byte is 0x80.
            if addr == 0x1fff8804:
                emu.mem_write(addr, b'\x80')

            return True

    def hook_code(self, mu, address, size, user_data):
        """ 
        """
        # DEBUG OUTPUT
        #print("(debug) Tracing instruction at {} (size = {})".format(hex(address), size))

        # Read a register
        #global self.done
        if address == 0x0:
            r2 = mu.reg_read(UC_ARM_REG_R2)
            print("R2 = {}".format(r2))
            #if r2 == X:
            #    self.done = 1

        # Write into a register
        if address == 0x0:
            mu.reg_write(UC_ARM_REG_R3, self.value)

        # Write in memory
        if address == 0x0:
            mu.mem_write(0x1792, b'\x02\x01')
    
    def _align_page_down(self, x):
        """
        :x ():
        :return ():
        """
        return x & ~(self.page_size - 1)

    def _align_page_up(self, x):
        """
        :x ():
        :return ():
        """
        return (x + self.page_size - 1) & ~(self.page_size-1)
