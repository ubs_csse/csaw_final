# CSAW Final

## Install our Custom Reverse Engineering Toolbox

Setup the environment from scratch :

```bash
# Download the latest Python verion here (3.7.4)
$ sudo dnf install python3

# Recommended : install a python virtualenv manager
$ sudo pip3 install pew

# Create a new virtualenv
$ sudo pew new csawenv

# Go to the csaw_package directory
$ cd csaw_package

# Install the package and its dependencies
$ pip install .
```

__Some Python packages are required to lauch most of the scripts, they are included as dependencies in our package__

*For this installation, we carefully recommand tu use a virtual environment manager.*

*This installation was made on a Fedora distribution. If you use another one, please refer to your distribution documentation.*

Additional important commands :

```bash
# List or change working virtualenvs
$ sudo pew workon <name>

# Exit a virtualenv
$ exit

# Delete the virualenv
$ sudo pew rm <name>
```