# 'Blue' Walkthrough

## Overview

This challenge looks like challenge 5 (`Dance`), the global structure is the
same. 8 bytes from the tag are used to generate a hash with the BLAKE-256 hash
function. The resulting hash is compared to an hard-coded one present in the
challenge function. If they are equals, the challenge is validated.
A clue is given here, a Base64 encoded string saying: `time to use the blue
tag`.

## Solution

__Step #1:__ Let's see the content of the blue tag.

The hard-coded hash does not match a known password here. With an Android
application like `MIFARE Classic Tool`, we dumped all the content of the blue
tag. Only the sector 15 contained data:

```bash
+Sector: 15
5D9295A4B98018B51DD9213C2AEF00CC
6ABC1FFBD341D88226FED70D4B6449DC
55C57178FD1034DA11E073BA2D6F2F45
------------FF078069FFFFFFFFFFFF
```

According to code refactoring done with Ghidra, we know that 8 bytes from RFID[960]
are used to generate a hash, that is a good point because the sector 15 start at
index 960. If we generate the hash of the first eight values of sector 15, we find
the same hash than the hard-coded one. So, we have the data and the location of data
inside the tag to validate this challenge... but the blue tag is not readable by
the RFID reader.

So, we programed the white tag with the first eight bytes of sector 15 but it
does not work better. It seems that the tags are not actually read because the
string "challenge sent" appears very quickly relative to other challenges.

__Step #2:__ Cloning the blue tag.

We looked into one possibility around Key A of sector 15 especially by cloning
the blue tag to safely change the key. For the cloning procedure, we used a
virgin tag with rewritable UID and the Android application `MIFARE Classic Tool`.
But these manipulations were not able to solve our problem, the cloned tag
was rejected, so the blue tag UID (Unique IDentifier) is blacklisted here.

__Step #3:__ We need to reverse our brain.

This challenge is the one on which we spent the most time. We tried to understand
the I2C communications between the different components and we were concerned
about some authentication functions and variables in both firmwares (Teensy and AVR).
Our suppositions was that we did not have enough privileges to impose the reading
of a tag in this challenge and that it was therefore necessary, for example, to
provide authentication keys to the system in order to increase privileges.
So, we went too far on this authentication topic and came back to basics with the
original idea that the solution involved just tag data and/or buttons of the
board. An interesting thing here is that when we initiated a challenge, we could
also trigger another action in the background like increasing the A or B button or
even change the selected challenge before submitting the tag.

__Step #4:__ Return to basics.

As we assumed that tags are not read in this challenge (reaction time of the board
really too fast when a tag is passed), we tried to launch another challenge before
moving to the challenge `blue` and it finally worked.

So, to solve this challenge, we start the challenge `code` on the board
(just preceding `blue`) and when the display shows "Please Scan
RFID Card", we push the Select challenge button to go in the `blue` challenge
in background. After that, we scan the white tag containing the eight bytes to
hash from RFID[960], the desired hash is produced and the challenge is validated.

## Validation

__Points:__ 200
__Hash:__ f7b26e5622acc85efd7c0e2b368b04e1ee2340b310fdb26dec9278b8c4860b14
