# -*- coding: utf8 -*-

import struct


chall_hash = [0x00]*32
chall_ready = False


def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def solve():
    rfid_content = [0x00]*1024
    
    alphabet = [
       0x20, 0x20, 0x20, 0x61,
       0x62, 0x63, 0x64, 0x65,
       0x66, 0x67, 0x68, 0x69,
       0x6a, 0x6b, 0x6c, 0x6d,
       0x6e, 0x6f, 0x70, 0x71,
       0x72, 0x73, 0x74, 0x75
    ]
    
    solution = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x20,
        0x20, 0x20, 0x20, 0x20,
        0x20, 0x20, 0x20, 0x20,
        0x20, 0x6d, 0x6f, 0x62,
        0x69, 0x6c, 0x65, 0x20,
        0x61, 0x62, 0x63, 0x64,
        0x65, 0x66, 0x67
    ]

    supposed = list(b'solved challenge mobile abcdefg')

    print('[+] Hardcoded array   : {}'.format(bytes(alphabet)))
    print('[+] Solution array    : {}'.format(bytes(solution)))
    print('[?] Supposed solution : {}\n'.format(bytes(supposed)))
    
    print('[*] Retrieving the offsets of each character of "challenge" in the alphabet ...')
    alpha_offsets = []
    word = b'challenge'
    for i in range(len(word)):
        for j in range(len(alphabet)):
            if word[i] == alphabet[j]:
                alpha_offsets.append((j%3, j//3))
                break
    print('[+] Corresponding offsets of the word "challenge" in the alphabet: {}\n'.format(alpha_offsets))
    
    print('[*] Retrieving the values...')
    values = []
    for c in alpha_offsets:
        for i in range(c[0]):
            values.append(c[1])
        values.append(c[1])
        values.append(0)
    print('[+] values found : {}'.format(values))

    for i in range(0, len(values), 2):
        rfid_content[int(i/2)+132] = (values[i] << 4) + values[i+1]

    display_rfid(rfid_content)

    reverse(rfid_content)


def reverse(rfid_content):
    """ESC 2019 Challenge n°4
    Open the door mobile
    """
    print('[*] Reading & processing RFID content...')

    values = [0x00]*30
    for i in range(0, 30, 2):
        offset = int(i/2) + 132
        values[i] = rfid_content[offset] >> 4
        values[i+1] = rfid_content[offset] & 15

    print('[+] RFID read values : {}'.format(values))

    alphabet = [
       0x20, 0x20, 0x20, 0x61,
       0x62, 0x63, 0x64, 0x65,
       0x66, 0x67, 0x68, 0x69,
       0x6a, 0x6b, 0x6c, 0x6d,
       0x6e, 0x6f, 0x70, 0x71,
       0x72, 0x73, 0x74, 0x75
    ]

    solution = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x20,
        0x20, 0x20, 0x20, 0x20,
        0x20, 0x20, 0x20, 0x20,
        0x20, 0x6d, 0x6f, 0x62,
        0x69, 0x6c, 0x65, 0x20,
        0x61, 0x62, 0x63, 0x64,
        0x65, 0x66, 0x67
    ]

    completion = 0
    index_chall = 7
    exit = False

    for j in range(1, 30):
        if not exit:
            if (values[j] == values[j-1]):
                completion += 1
            elif (values[j] == 0) or (values[j-1] == 0):
                if (values[j-1] != 0): 
                    solution[index_chall] = alphabet[completion + values[j-1]*3]
                    index_chall += 1
                completion = 0
            else:
                exit = True
            
    for k in range(0, 31):
        chall_hash[k] = solution[k]
    
    chall_hash[31] = ord(b'\x04')
    chall_ready = True

    print('[!] Flag :', bytes(chall_hash))


if __name__ == '__main__':
    solve()
