# 'Mobile' Walkthrough

## Overview

The goal of this challenge is to replace the string `"solved           mobile abcdefg"` by `"solved challenge mobile abcdefg"` in order to solve it. There is another string containing the beginning of an alphabet which enables us to write the word `"challenge"` instead of the spaces. 15 bytes of the tag are used to compute indexes, which are later used in a second loop to access a character from the alphabet string.

To accomplish this, 15 bytes are read from the RFID tag. On each byte, the loop that reads the RFID content performs a logical __Right Shift__ and a logical __AND__. Each operation stores the result in an array successively.

Then a second loop iterate over the resulting array. The values of the array are used as indexes to access the good character in the alphabet string.

## Solution

__Step #1:__ We can identify 2 hardcoded strings:
1. A partial alphabet string: "   abcdefghijklmnopqrstu"
2. An incomplete string: "solved           mobile abcdefg"

As seen in previous challenges, the valid `challHash` always start with `"solved challenge"`. Thus, we can supposed that the valid string should be `"solved challenge mobile abcdefg"`.
 
__Step #2:__ A first loop reads 15 bytes and performs a logical __Right Shift__ and a logical __AND__. For each byte, the first operation stores in an array the 4 most significant bits at index `i`, then the second one stores the 4 least significant bits at index `i+1`.

Here is what the source code looks like:

```C++
for (int i = 0; i < 30; i += 2) {
    values[i] = p.RFID[(i/2) + 132] >> 4;
    values[i + 1] = p.RFID[(i/2) + 132] & 0xf;
    i = i + 2;
}   
```

__Step #3:__ The second loop iterates over the values read from the RFID tag initialized in the previous loop. Values are processed as couples, each iteration work on the couple `(values[j], values[j-1])`. If each component of the couple has the same value, then a completion variable is incremented. Else, if one of the value in the couple is equal to `0`, then if `values[j-1]` is not equal to zero, the solution array is affected by the alphabet array at an offset determined by `completion + values[j-1] * 3`. Otherwise, an exit variable is set to true and no more couples will be processed until the loop ends.

Code source of the second loop:

```C++
int completion = 0;
int index_chall = 7; // Index on the first character
int exit = 0; 
for (int j = 1; j < 30; j++) {
    if (exit == 0) {
        if (values[j] == values[j-1]) {
            completion++;
        }
        else if (values[j] == 0 || values[j-1] == 0) {
            if (values[j-1] != 0) {
                solution[index_chall] = alphabet[completion+values[j-1]*3];
                index_chall += 1;
            }
            completion = 0;
        }
        else {
            exit = 1;
        }
    }
}
```

__Step #4:__ To solve this we need to find the offset for each character of the word `"challenge"` in the alphabet.

Here is the way to do it in Python:

```python
alphabet = b'   abcdefghijklmnopqrstu'
alpha_offsets = []
word = b'challenge'
for i in range(len(word)):
    for j in range(len(alphabet)):
        if word[i] == alphabet[j]:
            alpha_offsets.append((j%3, j//3))
            break

# output:
# [(2, 1), (1, 3), (0, 1), (2, 4), (2, 4), (1, 2), (1, 5), (0, 3), (1, 2)]
```

Each tuple represents the offset calculation `completion + values[j-1] * 3`. For example, the tuple `(2, 1)` is equivalent to `2 + (1 * 3) = 5`. In the alphabet, the caracter `'c'` is at offset 5.

__Step #5:__ Then, we need to find the good sequence in order to fit with all the conditions involed by the second loop.

In Python:

```python
values = []
for c in alpha_offsets:
    for i in range(c[0]):
        values.append(c[1]) # completion++
    values.append(c[1]) # values[j-1]
    values.append(0) # validate

# output:
# [1, 1, 1, 0, 3, 3, 0, 1, 0, 4, 4, 4, 0, 4, 4, 4, 0, 2, 2, 0, 5, 5, 0, 3, 0, 2, 2, 0]
```

To increment the variable `completion` we want each component of the couple `(values[j], values[j-1])` to be the same. Then, to validate an offset we have to put a zero at `values[j]` and the value to be multiplied by 3 at offset `values[j-1]`. It is necessary to put the value of `values[j-1]` for couples matching the condition.

__Step #6:__ Once we obtain the `values` array, we are able to determine the bytes to put in the RFID tag starting at offset 132 onwards.

In Python:

```python
for i in range(0, len(values), 2):
    rfid_content[int(i/2)+132] = (values[i] << 4) + values[i+1]

# output:
# [0x0, 0x0, 0x0, 0x0, 0x11, 0x10, 0x33, 0x1, 0x4, 0x44, 0x4, 0x44, 0x2, 0x20, 0x55, 0x3] # 8 [128..143]
# [0x2, 0x20, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0] # 9 [144..159]
```

## Validation

__Points:__ 100
__Hash:__ f2f3792453040e837e7e1584e72859bfaa6b8c09d73d185be53b35886b6455c2
