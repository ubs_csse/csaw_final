# -*- encoding: utf-8 -*-

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes


def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def solve():
    """CSAW'19 ESC Challenge n°5
    Open the door dance
    """
    rfid_content = [0x0]*1024

    known_hash = [
        0x5e, 0x88, 0x48, 0x98,
        0xda, 0x28, 0x04, 0x71,
        0x51, 0xd0, 0xe5, 0x6f,
        0x8d, 0xc6, 0x29, 0x27,
        0x73, 0x60, 0x3d, 0x0d,
        0x6a, 0xab, 0xbd, 0xd6,
        0x2a, 0x11, 0xef, 0x72,
        0x1d, 0x15, 0x42, 0xd8
    ]

    test = b'password'

    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(test)
    result_hash = digest.finalize()

    if result_hash == bytes(known_hash):
        print(f'[!] Success : {test}')
        rfid_content[147:147+len(test)] = list(test)
        display_rfid(rfid_content)
    else:
        print(f'[!] Fail : {test}')


if __name__ == '__main__':
    solve()