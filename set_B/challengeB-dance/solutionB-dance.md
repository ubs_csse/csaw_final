# `Dance` Walkthrough

## Overview

The `Dance` challenge reads 8 bytes from the RFID tag (from byte 147 to byte 154).
These 8 bytes are used to generate a hash with the BLAKE-256 hash function.
Next, the generated hash is compared to a hash hardcoded in the binary. If the comparison succeeds, the door is unlocked and the challenge is validated.

## Resolution flow

__Step #1:__ Using a reverse engineering tool (Ghidra) we can inspect the binary. We can see an hardcoded hash value `5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8`.

__Step #2:__ Hash generation.

Looking at Program Information in `File>Options for TeensyChallengeSetB.ino.elf>Program Information`, we easily see that a file named blake256.c is used in the SetB.

The command `strings TeensyChallengeSetB.ino.elf | grep blake` can be used here too in order to find this information (blake_256.h/blake_256.c). 

So, the BLAKE reference C implementation can be found here https://github.com/veorq/BLAKE/blob/master/blake256.c and a look at blake_256_update function give us these informations:

```c
void blake256_init( state256 *S )
void blake256_update( state256 *S, const uint8_t *in, uint64_t inlen )
void blake256_final( state256 *S, uint8_t *out )
```

If we focus only on the input and output of the hash function (`in` and `out`), we know that the 8 values (renamed here `RFID_values`) are passed to the hash function and the corresponding hash generated is stored in the byte array renamed `hash_value_to_find`.

```c
  blake_256_init(&BStack208);
  blake_256_update(&BStack208,RFID_values,8);
  blake_256_final(&BStack208,hash_value_to_find);
```

**So at this step, we know that we need to program the RFID tag with 8 values.**

__Step #3:__ Final comparison.

When a tag is passed to the board, 8 bytes inside the tag are used to generate a hash and this hash is compared to the hardcoded hash. If the two values are the same the challenge is validated.
A variable `k` is incremented each time the byte of both hash are equal. 

```c
  while (j < 0x20) {
    if (hash_value_to_find[j] == *(BYTE *)((int)hardcoded_hash_values + j)) {
      k = k + 1;
    }
    j = j + 1;
  }
```

So, if the generated hash is equal to the researched one, the challenge is validated.

```c
  if (k == 32) {
    n = 0;
    while (n < 0x1f) {
      challHash[n] = *(char *)((int)hardcoded_values + n);
      n = n + 1;
    }
  }
```

__Step #4:__ Find index of data in RFID tag.

After modification of the Challenge 5 function signature, **we observe that RFID values used here are stored from index RFID[147].**

```c
  i = 0;
  while (i < 8) {
    RFID_values[i] = (&RFID_147)[i];
    i = i + 1;
  }
```

__Step #5:__  Place the data in the tag.

In this step, some methods can be used to retrieve the original string.

The simple way to resolve this challenge is to try to crack the hash with an online tool like https://crackstation.net/
In this website, whe found that the string corresponding to the original hash is `password`.

Another way to resolve this challenge would have consists in writing a Python or a C code to generate multiple hashs from a dictionary of the most widespread strings. But here the first reflex to have is to try to crack the hash online.

**The solution for this challenge consists in storing the hex values of 'password' from RFID[147].**

```bash
[0, 0, 0, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0, 0, 0, 0, 0],  # 9 144-159 : password
```

## Validation

__Points:__ 130
__Hash:__ e631b32e3e493c51e5c2b22d1486d401c76ac83e3910566924bcc51b2157c837
