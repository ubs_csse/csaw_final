# 'Code' Walkthrough

## Overview

There is a hash function (H45H) using some hard-coded data concatenated with a
single byte from the RFID card. The goal is to discover what is the value of
that byte.

```c
data = 0x756a6d69;
uStack84 = 0x61727473;
uStack80 = 0x6d6f646e;
[...]
// Concatenation of the byte from the RFID tag and the hardcoded string
uStack48 = CONCAT13(p.RFID[139],0x217265);
[...]
// Hash computation
H45H(&HStack244);
Init(&HStack244);
Update(&HStack244,(uchar *)&data,0x2c);
Final(&HStack244);
// Comparison
n = strcmp(HStack244.digestChars,"242b461d0b97cca55e5d62372b770ab4");
```

_For some reason there is a difference of 16 between the RFID offset and the
actual offset we need to write in `sender.py`._

## Solution

__Step #1:__ A base64 encoded string is present and is potentially a hint:
`QmVyZ2VyIEtpbmc=` => `Berger King`. However we did not make any connection out
of this clue.

__Step #2:__

Since there is only one byte variable to compute the hash, there are only 256
possible hash for this program. We can very quickly go through them all till we
found the hash we are looking for. To do this we choose to instrument the binary
with Unicorn Engine, an emulation framework. That way we do not have to
reimplement the hash function. We need to catch the instruction reading from the
RFID tag (address `0x1B24`) to put our value into the correct register and then
read the memory where the computed hash is stored when we reached the comparison
(`0x1B70`). More information about how the instrumentation can be found in the
paper (Section IV. Firmware Emulation).

Output from `reverseB-code.py`:

```bash
[...]
[-] Trying VALUE = 76
[+] Setting up emulation environment..
[+] Binary mapped.
[+] Stack mapped.
[+] Registers set.
[+] All set. Now running..
[-] Trying to read at 0x1fff9358
[-] Trying to read at 0x1fff8804
[-] Trying to read at 0x1f806b0
R3 = 0x181b74. Hash = bytearray(b'242b461d0b97cca55e5d62372b770ab4')
[+] Solution found!
```

## Validation

__Points:__ 50
__Hash:__ 372ded6746e45ef7c8ad5a22c5738a4b5aa982da66bc8a426aa1cca830d05af3
