#!/usr/bin/env python3

import os

from unicorn import *
from unicorn.arm_const import *

BASE = 0
# Start of section .text
TEXT = 0x10000
STACK_ADDR = 0x0
STACK_SIZE = 1024 * 1024
PAGE_SIZE = 0x1000

# Information related to the function to emulate
FCT_START = 0x10F8
# Not the actual end, we don't need to execute everything
FCT_END = 0x11B6
VALUE_4C = 0
VALUE_4D = 0
VALUE_4E = 0

r2_t = 0
r3_p = 0
done = 0

REGS = [UC_ARM_REG_R0, UC_ARM_REG_R1, UC_ARM_REG_R2, UC_ARM_REG_R3,
		UC_ARM_REG_R4, UC_ARM_REG_R5, UC_ARM_REG_R6, UC_ARM_REG_R7,
		UC_ARM_REG_R8, UC_ARM_REG_R9, UC_ARM_REG_R10, UC_ARM_REG_R11,
		UC_ARM_REG_R12, UC_ARM_REG_PC, UC_ARM_REG_SP, UC_ARM_REG_LR,
		UC_ARM_REG_CPSR]


def ALIGN_PAGE_DOWN(x):
	return x & ~(PAGE_SIZE - 1)


def ALIGN_PAGE_UP(x):
	return (x + PAGE_SIZE - 1) & ~(PAGE_SIZE - 1)


def hook_mem_invalid(emu, access, addr, size, value, user_data):
	"""
	Catch illegal memory operation. Allocate desired memory and continue.
	"""
	if access == UC_MEM_WRITE_UNMAPPED:
		print("[-] Trying to write at {}".format(hex(addr)))
		# Trying to write to unmapped memory. Allocating some at target address
		# before resuming.
		s = ALIGN_PAGE_DOWN(addr)
		e = ALIGN_PAGE_UP(addr + size)
		emu.mem_map(s, e - s)

		# Return True to indicate we want to continue emulation
		return True
	elif access == UC_MEM_READ_UNMAPPED:
		print("[-] Trying to read at {}".format(hex(addr)))
		s = ALIGN_PAGE_DOWN(addr)
		e = ALIGN_PAGE_UP(addr + size)
		emu.mem_map(s, e - s)

		# There is a byte read at an unmapped memory address which is used to
		# determine __fdlib_version. This byte is 0x80.
		if addr == 0x1fff8eee:
			emu.mem_write(addr, b'\x01')


		return True


def hook_code(mu, address, size, user_data):
	# DEBUG
	# print("(debug) Tracing instruction at {} (size = {})".format(hex(address), size))

	# [R] keys[32]
	if address == 0x1124 or address == 0X114c:
		# Set R3
		print(f"setting keys 32 at {hex(address)} with {VALUE_4C}")
		mu.reg_write(UC_ARM_REG_R3, VALUE_4C)
	# [R] RFID[143]
	elif address == 0X1136:
		# Set R3
		print(f"setting RFID[143] at {hex(address)} with {VALUE_4D}")
		mu.reg_write(UC_ARM_REG_R3, VALUE_4D)
	# [R] RFID[144]
	elif address == 0x1142:
		print(f"setting RFID[144] at {hex(address)} with {VALUE_4E}")
		mu.reg_write(UC_ARM_REG_R3, VALUE_4E)

	if address == 0x1172:
		r3_i = mu.reg_read(UC_ARM_REG_R3)
		#print(f"target at {address} vaut {r3_i}")
	# read target and payload, expected target == payload.

	if address == 0x11B2:
		global r2_t
		r2_t = mu.reg_read(UC_ARM_REG_R2)
		# result = mu.mem_read(r3, 8)
		# result_int = int.from_bytes(result, byteorder='little')
		print("target = {}".format(r2_t))  # . Raw = {}. result = {}".format(hex(r3), result, result_int))
	if address == 0x11B4:
		global r3_p
		r3_p = mu.reg_read(UC_ARM_REG_R3)
		# result = mu.mem_read(r3, 8)
		# result_int = int.from_bytes(result, byteorder='little')
		if r3_p == r2_t:
			global done
			print("payload = {}".format(r3_p))  # . Raw = {}. result = {}".format(hex(r3), result, result_int))
			done = 1


def read_data(filename):
	with open(filename, "rb") as f:
		# Copy only from TEXT to synchronize instruction addresses with the
		# disassembly since .text section as a decalage of TEXT.
		f.seek(TEXT, 0)
		data = f.read()
		f.close()
	return data


def emulate(filename, size):
	print("[+] Setting up emulation environment..")
	mu = Uc(UC_ARCH_ARM, UC_MODE_THUMB)

	# Map the binary
	mu.mem_map(BASE, ALIGN_PAGE_UP(size))
	mu.mem_write(BASE, read_data(filename))
	# mu.mem_write(BASE, THUMB_CODE)
	#print("[+] Binary mapped.")

	# Map the stack
	global STACK_ADDR
	STACK_ADDR = ALIGN_PAGE_UP(BASE + size)
	mu.mem_map(STACK_ADDR, STACK_SIZE)
	#print("[+] Stack mapped.")

	# Set registers
	for r in REGS:
		mu.reg_write(r, 0)
	# -1024 so a function fetching arguments will not access unmapped memory.
	mu.reg_write(UC_ARM_REG_SP, STACK_ADDR + STACK_SIZE - 1024)
	#print("[+] Registers set.")

	# Register hooks and start emulation
	mu.hook_add(UC_HOOK_MEM_READ_UNMAPPED | UC_HOOK_MEM_WRITE_UNMAPPED, hook_mem_invalid)
	mu.hook_add(UC_HOOK_CODE, hook_code)
	print("[+] All set. Now running..")
	#print("FCT_START points to = {}".format(mu.mem_read(FCT_START, 50)))
	#print("FCT_END = {}".format(FCT_END))

	# Note we start at ADDRESS | 1 to indicate THUMB mode.
	try:
		mu.emu_start(FCT_START | 1, FCT_END)
	except ValueError:
		print(f"error : {ValueError.with_traceback()} ")
		return


if __name__ == "__main__":

	filename = "/home/pfontaine/csaw_esc_2019/Challenge Releases/ChallengeSetC/TeensyChallengeSetC.ino.elf"
	size = os.path.getsize(filename)
	VALUE_4C = 0
	VALUE_4D = 65
	VALUE_4E = 97
	# emulate(filename, size)

	print("\n\nStarting attempt 4\n")
	for i in range(0, 257):
		VALUE_4C = i
		if done:
			break
		for j in range(65, 257):
			if done:
				break
			VALUE_4D = j
			for k in range(0, 257):
				VALUE_4E = k
				print("\n[-] Trying VALUE_4C = {} | VALUE_4D = {} | VALUE_4E = {}".format(VALUE_4C, VALUE_4D, VALUE_4E))
				emulate(filename, size)
				if done:
					break


