# 'Break' Walkthrough

## Overview

This challenge is all about an LFSR (Linear Feedback Shift Register). It consists in an algorithm using XOR and SHIFT operations. Due to its simplicity of implementation, LFSR are commonly used in hardware.

## Solution

__Step #1:__

In this challenge, the LFSR is running over 1622 steps. At the end, the output is compared with mixed values from the RFID tag.
Since there are 2 values read from the tag and one from `Keys` array, our first approach will be to instrument the program using emulation.

From a computational point of view, emulation is a heavy task, and when we do the math we can tell that computing 256^3 different combinations  will be time consuming. However since `Keys` array has not be used until this moment we should maybe try something setting this value to 0, and to compute 256^2 should be relatively quick .

__Step #2__:

In order instrument the program, we do need to spot some strategic points.

| label                | addr   |
| -------------------- | ------ |
| Start of function    | 0x10F8 |
| Instruction targeted | 0x11BC |

__Step #3:__

Then we have to figure out where to inject our values in registers. By digging in the assembly code we can find the following lines.

```asm
00001120 97 f8 79 34     ldrb.w     r3,[r7,#p.keys[32]]
00001124 1b 02           lsl        r3,r3,#0x8
...
00001132 97 f8 e8 30     ldrb.w     r3,[r7,#p.RFID[143]]
00001136 1b 02           lsl        r3,r3,#0x8
...
0000113e 97 f8 e9 30     ldrb.w     r3,[r7,#p.RFID[144]]
00001142 1b b2           sxth       r3,r3
...
00001148 97 f8 79 34     ldrb.w     r3,[r7,#p.keys[32]]
0000114c 1b 01           lsl        r3,r3,#0x4
```

Thus, in our script in the `hook_code` method we will set those different condition statements

```python
if address == 0x1124 or address == 0X114c:
		mu.reg_write(UC_ARM_REG_R3, VALUE_4C)
elif address == 0X1136:
		mu.reg_write(UC_ARM_REG_R3, VALUE_4D)
elif address == 0x1142:
		mu.reg_write(UC_ARM_REG_R3, VALUE_4E)
```

__Step #4:__

The last part for writing our script is to find where values are compared, below you will find the corresponding instructions.

```asm
000011b0 fa 8e           ldrh       r2,[r7,#target]
000011b2 fb 8c           ldrh       r3,[r7,#local_32]
000011b4 9a 42           cmp        r2,r3
```

Now we can read registers at addresses found.

```python
if address == 0x11B2:
		r2_t = mu.reg_read(UC_ARM_REG_R2)
if address == 0x11B4:
		r3_p = mu.reg_read(UC_ARM_REG_R3)
```

__Step #5:__

Here is the output of our exploit, we can clearly see the values required to have a match between the LFSR output and the input of our function.

```python
[+] Setting up emulation environment..
[+] All set. Now running..
[-] Trying to read at 0x181021
setting keys 32 at 0x1124 with 0
setting RFID[143] at 0x1136 with 65
setting RFID[144] at 0x1142 with 97
setting keys 32 at 0x114c with 0
target = 16737
payload = 16737
```

## Validation

__Points:__ 70
__Hash:__ 2d3448f09329f453e6f3a5403d89c061a9dabfbb9103ad6b8cc86d16345a7547
