# 'Recess' Walkthrough

## Overview

This challenge has to do with a Cyclic Redundancy Check (CRC) algorithm. It is an error-detecting code generally used in networks to detect errors in the data transmission.

The challenge function reads 4 bytes from the RFID tag at offset 161 onwards. The objective of this challenge is to retrieve the correct 4 bytes for which the CRC result is equal to `0x36476684`. 

## Solution

__Step #1:__ In the challenge function, 4 bytes of the RFID tag are put in a char array of size 4. This array is then passed as argument to the `rc_crc32()` function. If the result is equal to `0x36476684`, then the challenge is validated.

We easily find the source code of the `rc_crc32()` that is used in this challenge on the Internet ([link](https://rosettacode.org/wiki/CRC-32)).

__Step #2:__ In order to find the corresponding CRC result, we need to test all the bytes combinations. The values a byte can take is from `0x0` to `0xFF` (0 to 256 in decimal). Thus, we have `256 x 256 x 256 x 256 = 4,294,967,296` possibilities to test.

__Step #3:__ To solve this challenge effectively, we will use an OpenMP program in order to share the work among the logical processors of our machine.

Here is what the code looks like in `src/solveChallengeRecess.c`:

```C
unsigned int i, j, k, l;
char c[4];

double start = omp_get_wtime();

#pragma omp parallel for private(i, j, k, l, c) schedule(static)
for (i = 0; i < 256; i++) {
    for (j = 0; j < 256; j++) {
        for (k = 0; k < 256; k++) {
            for (l = 0; l < 256; l++) {
                c[0] = i;
                c[1] = j;
                c[2] = k;
                c[3] = l;
                if (rc_crc32(0, c, 4) == 0x36476684) {
                    printf("[!] Success :\n");
                    printf("    [+] RFID[161] = 0x%02x \n", c[0]);
                    printf("    [+] RFID[162] = 0x%02x \n", c[1]);
                    printf("    [+] RFID[163] = 0x%02x \n", c[2]);
                    printf("    [+] RFID[164] = 0x%02x \n", c[3]);
                }
            }
        }
    }
}

double end = omp_get_wtime();

fprintf(stdout, "Wall time = %.4g sec\n", end - start);
```

We have four nested loops which allow us to test every possible characters for the char array.

After running the program `bin/solveChallengeRecess`, we have the following output:

```
[!] Success:
    [+] RFID[161] = 0x67
    [+] RFID[162] = 0x30
    [+] RFID[163] = 0x30
    [+] RFID[164] = 0x64
Wall time = 14.55 sec
```

In ASCII, this gives us the string `"g00d"` and the compute time on our machine was 14.55 seconds.

In the context of this challenge, we could have imagine that the results would represent ASCII characters. Then the possibilities to test would have been reduced to 268,435,456.

Finally, we can fill the `senderC-recess.py` with the resulting values.

## Build Note

To build the exploit, here are the instructions:
```bash
make
./bin/solveChallengeRecess
```

## Validation

__Points:__ 100
__Hash:__ 370815b8d8fde829f5c35f893d0b4139d61a775baa4181fcac1fffe014bde9ea
