#include <omp.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
 

uint32_t rc_crc32(uint32_t crc, const char *buf, size_t len) {
	static uint32_t table[256];
	static int have_table = 0;
	uint32_t rem;
	uint8_t octet;
	int i, j;
	const char *p, *q;
 
	/* This check is not thread safe; there is no mutex. */
	if (have_table == 0) {
		/* Calculate CRC table. */
		for (i = 0; i < 256; i++) {
			rem = i;  /* remainder from polynomial division */
			for (j = 0; j < 8; j++) {
				if (rem & 1) {
					rem >>= 1;
					rem ^= 0xedb88320;
				} else
					rem >>= 1;
			}
			table[i] = rem;
		}
		have_table = 1;
	}
 
	crc = ~crc;
	q = buf + len;
	for (p = buf; p < q; p++) {
		octet = *p;  /* Cast to unsigned octet. */
		crc = (crc >> 8) ^ table[(crc & 0xff) ^ octet];
	}

	return ~crc;
}


int main() {
	unsigned int i, j, k, l;
	char c[4];

	double start = omp_get_wtime();

	#pragma omp parallel for private(i, j, k, l, c) schedule(static)
	for (i = 0; i < 256; i++) {
		for (j = 0; j < 256; j++) {
			for (k = 0; k < 256; k++) {
				for (l = 0; l < 256; l++) {
					c[0] = i;
					c[1] = j;
					c[2] = k;
					c[3] = l;
					if (rc_crc32(0, c, 4) == 0x36476684) {
						printf("[!] Success : %c%c%c%c\n", c[0], c[1], c[2], c[3]);
						printf("    [+] RFID[161] = 0x%02x \n", c[0]);
						printf("    [+] RFID[162] = 0x%02x \n", c[1]);
						printf("    [+] RFID[163] = 0x%02x \n", c[2]);
						printf("    [+] RFID[164] = 0x%02x \n", c[3]);
					}
				}
			}
		}
	}

	// #pragma omp parallel for private(i, j, k, l, c) schedule(static)
	// for (i = 0; i < 128; i++) {
	// 	for (j = 0; j < 128; j++) {
	// 		for (k = 0; k < 128; k++) {
	// 			for (l = 0; l < 128; l++) {
	// 				c[0] = i;
	// 				c[1] = j;
	// 				c[2] = k;
	// 				c[3] = l;
	// 				if (rc_crc32(0, c, 4) == 0x36476684) {
	// 					printf("[!] Success :\n");
	// 					printf("    [+] RFID[161] = 0x%02x \n", c[0]);
	// 					printf("    [+] RFID[162] = 0x%02x \n", c[1]);
	// 					printf("    [+] RFID[163] = 0x%02x \n", c[2]);
	// 					printf("    [+] RFID[164] = 0x%02x \n", c[3]);
	// 				}
	// 			}
	// 		}
	// 	}
	// }

	double end = omp_get_wtime();

	fprintf(stdout, "Wall time = %.4g sec\n", end - start);

    return 0;
}
