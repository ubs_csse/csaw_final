# -*- coding: utf8 -*-

import binascii
import itertools
import string
import time
import multiprocessing as mp
import collections


chall_hash = [0x00]*32
chall_ready = False

def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def solve():
    """ESC 2019 Challenge n°11
    Open the door recess
    """
    rfid_content = [0x0]*1024

    hardcoded_array = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c,
        0x65, 0x6e, 0x67, 0x65,
        0x20, 0x72, 0x65, 0x63,
        0x65, 0X73, 0x73, 0x20,
        0x61, 0x62, 0x63, 0x64,
        0x65, 0x66 
    ]

    print('[+] Hardcoded array : {}'.format(bytes(hardcoded_array)))

    rfid_content[161] = 0x67
    rfid_content[162] = 0x30
    rfid_content[163] = 0x30
    rfid_content[164] = 0x64

    display_rfid(rfid_content)

    reverse(rfid_content)


def reverse(rfid_content):
    hardcoded_array = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c,
        0x65, 0x6e, 0x67, 0x65,
        0x20, 0x72, 0x65, 0x63,
        0x65, 0X73, 0x73, 0x20,
        0x61, 0x62, 0x63, 0x64,
        0x65, 0x66, 0x67
    ]

    input_crc = [0x0]*4
    for i in range(0, 4):
        input_crc[i] = rfid_content[i+161]

    print('[+] Read values from RFID : {}'.format(bytes(input_crc)))

    crc_result = hex(binascii.crc32(bytes(input_crc)))
    if (crc_result == '0x36476684'):
        for i in range(0, 31):
            chall_hash[i] = hardcoded_array[i]
    else:
        for k in range(0, 31):
            chall_hash[k] = 0xff
    
    chall_hash[31] = ord('\v')
    chall_ready = True

    print('[!] Flag :', bytes(chall_hash))


if __name__ == '__main__':
    solve()