#!/usr/bin/env python3

import os

from unicorn import *
from unicorn.arm_const import *

BASE = 0
# Start of section .text
TEXT = 0x10000
STACK_ADDR = 0x0
STACK_SIZE = 1024*1024
PAGE_SIZE = 0x1000

# Information related to the function to emulate
FCT_START = 0xCEC
# Not the actual end, we don't need to execute everything
FCT_END = 0xED8

# OK to change the char. Need to find correct sequence to store in challHash.
#VALUE_A = [ 0x30, 0x40, 0x00, 0x40, 0xFF, 0x00, 0xFF ]
VALUE_A = [0x30, 0x40, 0x00,
           0x40, 0xFF, 0x00,
           
           0x30, 0x41, 0x31,
           0x41, 0xFF, 0x00,
           
           0x30, 0x42, 0x00,
           0x42, 0xFF, 0x00,
           
           0x32, 0x1, 0x00,
           0x32, 0x3, 0x00,
           
           0x32, 0x7, 0x00,
           0x32, 0x9, 0x00,
           
           0x32, 0xd, 0x00,
           0x32, 0xf, 0x00,
           0x31, 0x33, 0x00]

VALUE_B = [ 0x3, 0xff, 0xfd, 0xf6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00 ]
index_a = 0
index_b = 0

REGS = [ UC_ARM_REG_R0, UC_ARM_REG_R1, UC_ARM_REG_R2, UC_ARM_REG_R3,
         UC_ARM_REG_R4, UC_ARM_REG_R5, UC_ARM_REG_R6, UC_ARM_REG_R7,
         UC_ARM_REG_R8, UC_ARM_REG_R9, UC_ARM_REG_R10, UC_ARM_REG_R11,
         UC_ARM_REG_R12, UC_ARM_REG_PC, UC_ARM_REG_SP, UC_ARM_REG_LR,
         UC_ARM_REG_CPSR ]

def ALIGN_PAGE_DOWN(x):
    return x & ~(PAGE_SIZE - 1)

def ALIGN_PAGE_UP(x):
    return (x + PAGE_SIZE - 1) & ~(PAGE_SIZE-1)

def hook_mem_invalid(emu, access, addr, size, value, user_data):
    """
    Catch illegal memory operation. Allocate desired memory and continue.
    """
    if access == UC_MEM_WRITE_UNMAPPED:
        print("[-] Trying to write at {}".format(hex(addr)))
        # Trying to write to unmapped memory. Allocating some at target address
        # before resuming.
        s = ALIGN_PAGE_DOWN(addr)
        e = ALIGN_PAGE_UP(addr+size)
        emu.mem_map(s, e - s)

        # Return True to indicate we want to continue emulation
        return True
    elif access == UC_MEM_READ_UNMAPPED:
        print("[-] Trying to read at {}".format(hex(addr)))
        s = ALIGN_PAGE_DOWN(addr)
        e = ALIGN_PAGE_UP(addr+size)
        emu.mem_map(s, e - s)

        return True

def hook_code(mu, address, size, user_data):
    global index_a
    global index_b
    # DEBUG
    #print("(debug) Tracing instruction at {} (size = {})".format(hex(address), size))

    # Replace the byte read from the RFID card packet.
    if address == 0xD7E:
        # Set R3
        if index_a < len(VALUE_A):
            mu.reg_write(UC_ARM_REG_R3, VALUE_A[index_a])
        else:
            mu.reg_write(UC_ARM_REG_R3, 0)
        index_a += 1
    elif address == 0xDBE:
        # Set R3
        mu.reg_write(UC_ARM_REG_R3, VALUE_B[index_b])
        index_b += 1

    # Read variables in the interpreter loop
    if address == 0xE2C:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("local_3C = {}".format(hex(r3)))
    elif address == 0xE40:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("local_40 = {}".format(hex(r3)))
    elif address == 0xE54:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("local_44 = {}".format(hex(r3)))

    # Check what is put into challHash
    if address == 0xE8C:
        r1 = mu.reg_read(UC_ARM_REG_R1)
        print("(challHash) R1 = {}\n".format(r1))
    if address == 0xEA6:
        r1 = mu.reg_read(UC_ARM_REG_R1)
        print("(R1 after sub) S[local_40] = {}".format(hex(r1)))

def read_data(filename):
    with open(filename, "rb") as f:
        # Copy only from TEXT to synchronize instruction addresses with the
        # disassembly since .text section as a decalage of TEXT.
        f.seek(TEXT, 0)
        data = f.read()
        f.close()
    return data

def emulate(filename, size):
    print("[+] Setting up emulation environment..")
    mu = Uc(UC_ARCH_ARM, UC_MODE_THUMB)

    # Map the binary
    mu.mem_map(BASE, ALIGN_PAGE_UP(size))
    mu.mem_write(BASE, read_data(filename))
    #mu.mem_write(BASE, THUMB_CODE)
    print("[+] Binary mapped.")

    # Map the stack
    global STACK_ADDR
    STACK_ADDR = ALIGN_PAGE_UP(BASE + size)
    mu.mem_map(STACK_ADDR, STACK_SIZE)
    print("[+] Stack mapped.")

    # Set registers
    for r in REGS:
        mu.reg_write(r, 0)
    # -1024 so a function fetching arguments will not access unmapped memory.
    mu.reg_write(UC_ARM_REG_SP, STACK_ADDR + STACK_SIZE-1024)
    print("[+] Registers set.")

    # Register hooks and start emulation
    mu.hook_add(UC_HOOK_MEM_READ_UNMAPPED | UC_HOOK_MEM_WRITE_UNMAPPED, hook_mem_invalid)
    mu.hook_add(UC_HOOK_CODE, hook_code)
    print("[+] All set. Now running..")
    #print("FCT_START points to = {}".format(mu.mem_read(FCT_START, 50)))
    #print("FCT_END = {}".format(FCT_END))

    # Note we start at ADDRESS | 1 to indicate THUMB mode.
    mu.emu_start(FCT_START | 1, FCT_END)

if __name__ == "__main__":

    filename = "/home/math/csaw/final/ChallengeSetC/TeensyChallengeSetC.ino.elf"
    size = os.path.getsize(filename)

    #print("\n\nStarting attempt 4\n")
    #for i in range(256-VALUE_4C):
    #    for j in range(256):
    #        print("\n[-] Trying VALUE_4C = {} | VALUE_4D = {}".format(VALUE_4C, VALUE_4D))
    #        emulate(filename, size)
    #        VALUE_4D += 1
    #    VALUE_4D = 0
    #    VALUE_4C += 1

    print("\n[-] Trying VALUE_A = {} | VALUE_B = {}".format(VALUE_A, VALUE_B))
    emulate(filename, size)
