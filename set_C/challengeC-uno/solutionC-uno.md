# 'Uno' Walkthrough

## Overview

There is a hint for this challenge: “Is OISC 1337?” 

__OISC__ = One-Instruction Set Architecture.

As its name implied it is an architecture within which all computations are done
with only one instruction. That instruction may vary.

References:
- Subleq: https://esolangs.org/wiki/Subleq
- Subleq compiler: http://mazonka.com/subleq/online/hsqjs.cgi

## Solution

__Step #1:__

The following piece of code reproduces the behavior of the __SUBLEQ__ (SUBtract and
branch if Less-than or EQual to zero) instruction. This code corresponds to the
interpreter of the SUBLEQ virtual machine which will interpret the instruction
loaded into the array containing our data.

```c
// exit the VM
if (addr_data == -1) break;
// store a byte
if ((addr_dest == -1) && (len_hash < 0x1f)) {
    challHash[len_hash] = (char)buffer[addr_data];
    len_hash = len_hash + 1;
}
// change a byte from the array
else {
    buffer[addr_dest] = buffer[addr_dest] - buffer[addr_data];
    if (buffer[addr_dest] < 1) {
        index = additional_data;
    }
}
```

The array is filled with data from the RFID card through two loops:
- The first 0x30 bytes of data from offset `0x200`.
- The second 0x10 bytes of data from offset `0x240`.

We have a total of 0x40 bytes to store our SUBLEQ program.

The `challHash` string is encoded (“vroy…”). Basically what we have to do is
subtract 3 to every chars to get the original success message back (“solved…”).

0x00                           0x30               0x40                 0x5F
|                               |                  |                    |
v                               v                  v                    v
-------------------------------------------------------------------------
| p.RFID[0x200]...              | p.RFID[0x240]... | "vroyhg..."        |
-------------------------------------------------------------------------

__Step #2:__

From there we can look for a SUBLEQ compiler but a simple for loop takes about
600 bytes.  We only have 64. Plus we would have to modify it so it would match
the offsets we want to modify (a.k.a. the `challHash` encoded string) and delete
the unecessary part, so anyway doing it manually is the better option here.

__Step #3:__

What we need to do is to write a SUBLEQ program that will subtract 3 to each
character of `vroyhg..`.

Here is an example of a valid instruction to change a character:

```bash
1: 0x30, 0x40, 0x00, # buffer[0x40] -= buffer[0x30]
2: 0x40, 0xFF, 0x00, # challHash[i] = stack[0x40]
```

Line 1: `0x30` is the address (in the buffer) of the value we want to subtract
to the value at `0x40`. The 3rd operand is not used for this one.

Line 2: `0x40` is the address containing the value modified on line 1 and `0xFF`
as second operand means that we want to save it at `challHash[i]`. The 3rd operand
is not use here neither.

So it takes 6 bytes to modify 1 character of the string. The problem is that we
only have 48 bytes to store our program which is not enough to do all the
modifications at once. To circumvent this restriction we have to make our code
self-modifying so it can update itself after a while.

We used the 0x30->0x40 to store our instructions and 0x40 to store our data.
That way we just have to worry about changing some of the instructions and be
guarenteed to have the data always at the same addresses. One iteration of our
loop modify 3 characters of the string, then one bytes of each instructions, so
we need to execute that 11 times in total.

To loop we need a counter instantiated at -10 (11 iterations), that will be incremented by
one thanks to our last instructions (- (-1)). -10 because it will satisfy the
comparison and re-initialize `index` to `additional_data` (the 3rd operand).

```c
[...]
    buffer[addr_dest] = buffer[addr_dest] - buffer[addr_data];
    // Condition we need to satisfy in order to loop
    if (buffer[addr_dest] < 1) {
        index = additional_data;
    }
```

Full SUBLEQ program:
```bash
# Data:
#   buffer[0x30] = 0x3
#   buffer[0x31] = 0xff (-1)
#   buffer[0x32] = 0xfd (-5)
#   buffer[0x33] = 0xf5 (-11)

0x30, 0x40, 0x00, # buffer[0x40] -= buffer[0x30]
0x40, 0xFF, 0x00, # challHash[i] = buffer[0x40]

# After the last char has been modified, the interpreter will # loop on these 2
# instructions (0x41 would have become 0x5e) and the subtraction results will #
# always be negative, so we put 0x31 in the 3rd operand to indicate the jump
# destination when # that happens (it will break the loop since 0x31 points to
# 0xff => next instruction will # have 0xff as addr_data).
0x30, 0x41, 0x31,
0x41, 0xFF, 0x00,

0x30, 0x42, 0x00,
0x42, 0xFF, 0x00,

# At this point 3 chars of the final hash done, using 18 bytes of subleq code
# Time for the self-modifying part. Add 3 to offset referencing the string.

0x32, 0x1, 0x00,
0x32, 0x3, 0x00,

0x32, 0x7, 0x00,
0x32, 0x9, 0x00,

0x32, 0xd, 0x00,
0x32, 0xf, 0x00,

0x31, 0x33, 0x00 # jump back to the beginning, buffer[0x33] = counter - (-1)

# Total bytes = 39/48
```

## Validation

__Points:__ 200
__Hash:__ a990debdea0f626803f99286296468acb75bb6be5d118c954d874d4b92ed9059
