# -*- coding: utf8 -*-

import struct
import time

chall_hash = [0x00]*32
chall_ready = False

def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def solve():
    """ESC 2019 Challenge n°9
    Open the door game
    """
    rfid_content = [0x00]*1024

    hardcoded_array = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c,
        0x65, 0x6e, 0x67, 0x65,
        0x20, 0x67, 0x61, 0x6d,
        0x65, 0x20, 0x61, 0x62,
        0x63, 0x64, 0x65, 0x66,
        0x67, 0x68, 0x69
    ]

    grid = [
        0x78, 0x78, 0x5f,
        0x5f, 0x6f, 0x5f,
        0x5f, 0x5f, 0x5f
    ]

    print('[+] Hardcoded array : {}'.format(bytes(hardcoded_array)))
    time.sleep(2)
    print('[+] Original grid (draw game) :')
    for i in range(0, len(grid), 3):
        print('   ', ' '.join([chr(u) for u in grid[i:i+3]]))
    time.sleep(5)
    wanted = [
        0x78, 0x78, 0x6f,
        0x6f, 0x6f, 0x78,
        0x78, 0x6f, 0x78
    ]

    print('[+] Wanted final grid : {}')
    for i in range(0, len(wanted), 3):
        print('   ', ' '.join([chr(u) for u in wanted[i:i+3]]))

    row_indexes = []
    col_indexes = []
    for i in range(0, len(grid)):
        if i != 4 and wanted[i] == 0x6f:
            row_indexes.append(i//3)
            col_indexes.append(i%3)

    for i in range(0, len(col_indexes)):
        rfid_content[i+156] = (row_indexes[i] << 4) + col_indexes[i]
    print("[!] Solution found !")
    # display_rfid(rfid_content)


if __name__ == '__main__':
    solve()
