# 'Game' Walkthrough

## Overview

The game represented by this challenge is a classic Tic-Tac-Toe. It is a famous paper-and-pencil game for two players (`X` and `O`), who take turns marking the spaces in a 3*3 grid. The player who succeeds in placing 3 of its marks in a horizontal, vertical, or diagonal row is the winner. Here our opponent is the RFID reader.

In the challenge function, the grid is already initialized with 3 marks. The opponent has the cross mark, then we have the nought mark. Our turn start by reading the byte in the RFID tag at offset 156. This byte correspond to the coordinate where to put the nought mark in the grid. Then, it is the turn of the opponent who evaluates the grid and then perform the best move. The game ends after performing 3 turns each. We solve the challenge if we win or if it is a draw game.

## Solution

__Step #1:__ Using Ghidra, we are able to determine the string `"xx__o____"`. Thanks to the name of the challenge and the string found, we understand that it will be a Tic-Tac-Toe game. 

The grid is already initialized with 3 marks:

```
 x | x | _ 
---+---+---
 _ | o | _ 
---+---+---
 _ | _ | _ 
```

After some tests, we quikly see that we can not place three nought marks in order to win the game. Thus, it must be a draw game to valid this challenge beacause best plays from both parties leads to a draw game.

Here is the way to get a draw game:

```
 x | x | _      x | x | o      x | x | o      x | x | o      x | x | o 
---+---+---    ---+---+---    ---+---+---    ---+---+---    ---+---+---
 _ | o | _      _ | o | _      _ | o | _      o | o | _      o | o | x 
---+---+---    ---+---+---    ---+---+---    ---+---+---    ---+---+---
 _ | _ | _      _ | _ | _      x | _ | _      x | _ | _      x | _ | _ 

 Opponent          Tag          Opponent         Tag          Opponent
```

At this point, we have two possibilities:

```
 x | x | o       x | x | o 
---+---+---     ---+---+---
 o | o | x   or  o | o | x 
---+---+---     ---+---+---
 x | o | _       x | _ | o 

   Tag            Tag
```

Finally the oppopent place its last mark:

```
 x | x | o       x | x | o 
---+---+---     ---+---+---
 o | o | x   or  o | o | x 
---+---+---     ---+---+---
 x | o | x       x | x | o 

 Opponent       Opponent
```

__Step #2:__ Now that we know our possible moves, we are able to determine our three coordinates where to put a nought. Each coordinate is represented by a byte of the RFID tag. The 4 most significant bits of the byte correspond to the row number and the 4 least significant bits correspond to the column number of the grid.

In our first turn, we place a nought at row `0` and column `2`, so the byte read at offset 156 will be `0x02`.

We do the same thing for all noughts placed in step 2 and we obtain: 

```C++
p.RFID[156] = 0x02;
p.RFID[157] = 0x10;
p.RFID[158] = 0x21; // or 0x22
```

Finally, we can place those values in the `senderC-game.py`.


## Validation

__Points:__ 150
__Hash:__ a536829856d84ccd53ff8bcf534a65c5678bdbe9ce20f78407e1c987ba517e8a
