#!/usr/bin/env python3

import os
import struct

from unicorn import *
from unicorn.arm_const import *

BASE = 0
# Start of section .text
TEXT = 0x10000
STACK_ADDR = 0x0
STACK_SIZE = 1024*1024
PAGE_SIZE = 0x1000

# Information related to the function to emulate
FCT_START = 0x71C
# Not the actual end, we don't need to execute everything
FCT_END = 0x866
VALUE_A = [0, 0, 0, 0, 0, 0, 0, 0xff]
#VALUE_B = [0x00, 0x00, 0x00, 0x00, 0x90, 0xD0, 0x75, 0x76, 0x77, 0x78, 1, 2, 3, 4, 5, 0, 0, 0]
VALUE_B = [0x00, 0x1f, 0xff, 0x88, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0, 0, 3, 0]
ITER_1 = 0
ITER_2 = 0
DONE = 0

REGS = [ UC_ARM_REG_R0, UC_ARM_REG_R1, UC_ARM_REG_R2, UC_ARM_REG_R3,
         UC_ARM_REG_R4, UC_ARM_REG_R5, UC_ARM_REG_R6, UC_ARM_REG_R7,
         UC_ARM_REG_R8, UC_ARM_REG_R9, UC_ARM_REG_R10, UC_ARM_REG_R11,
         UC_ARM_REG_R12, UC_ARM_REG_PC, UC_ARM_REG_SP, UC_ARM_REG_LR,
         UC_ARM_REG_CPSR ]

def ALIGN_PAGE_DOWN(x):
    return x & ~(PAGE_SIZE - 1)

def ALIGN_PAGE_UP(x):
    return (x + PAGE_SIZE - 1) & ~(PAGE_SIZE-1)

def hook_mem_invalid(emu, access, addr, size, value, user_data):
    """
    Catch illegal memory operation. Allocate desired memory and continue.
    """
    if access == UC_MEM_WRITE_UNMAPPED:
        print("[-] Trying to write at {}".format(hex(addr)))
        # Trying to write to unmapped memory. Allocating some at target address
        # before resuming.
        s = ALIGN_PAGE_DOWN(addr)
        e = ALIGN_PAGE_UP(addr+size)
        emu.mem_map(s, e - s)

        # Return True to indicate we want to continue emulation
        return True
    elif access == UC_MEM_READ_UNMAPPED:
        print("[-] Trying to read at {}".format(hex(addr)))
        s = ALIGN_PAGE_DOWN(addr)
        e = ALIGN_PAGE_UP(addr+size)
        emu.mem_map(s, e - s)

        # There is a byte read at an unmapped memory address which is used to
        # pad the hash. This byte is 0x80.
        #if addr == 0x1fff8804:
        #    emu.mem_write(addr, b'\x80')

        return True

def hook_code(mu, address, size, user_data):
    # DEBUG
    #print("(debug) Tracing instruction at {} (size = {})".format(hex(address), size))
    # Check *local_68 accessing
    if address == 0x7E0:
        r2 = mu.reg_read(UC_ARM_REG_R2)
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("(debug) Trying to write {} at {}".format(hex(r2), hex(r3)))

    # Replace bytes read from tag (overflow of local_78[])
    global ITER_1
    if address == 0x77C:
        mu.reg_write(UC_ARM_REG_R1, VALUE_A[ITER_1])
        ITER_1 += 1

    # Replace one byte read from tag (enable debug mode)
    if address == 0x792:
        mu.reg_write(UC_ARM_REG_R3, 1)

    # Replace byte read to overwrite the stack (overwriting of 'local_68' and
    # 'privilege')
    global ITER_2
    if address == 0x7C8:
        mu.reg_write(UC_ARM_REG_R1, VALUE_B[ITER_2])
        ITER_2 += 1

    # Check that we are writing what we want where we want to 
    if address == 0x7D4:
        r2 = mu.reg_read(UC_ARM_REG_R2)
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("writing {} at {}".format(hex(r2), hex(r3)))

    if address == 0x836:
        r2 = mu.reg_read(UC_ARM_REG_R2)
        print("&privilege = {}".format(hex(r2)))
        print("privilege = {}".format( mu.mem_read(r2, 4) ))
    elif address == 0x84e:
        r2 = mu.reg_read(UC_ARM_REG_R2)
        print("&local_74 = {}".format(hex(r2)))
        print("local_74 = {}".format( mu.mem_read(r2, 4) ))
    elif address == 0x784:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("&local_78 = {}".format(hex(r3)))
    elif address == 0x828:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("&local_64 = {}".format(hex(r3)))
        print("local_64 = {}".format( mu.mem_read(r3, 4) ))
    elif address == 0x80e:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("&local_68 = {}".format(hex(r3)))
        print("local_68 = {}".format( mu.mem_read(r3, 4) ))

    # Check 'privilege'
    if address == 0x862:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("privilege = {}".format(hex(r3)))

def read_data(filename):
    with open(filename, "rb") as f:
        # Copy only from TEXT to synchronize instruction addresses with the
        # disassembly since .text section as a decalage of TEXT.
        f.seek(TEXT, 0)
        data = f.read()
        f.close()
    return data

def emulate(filename, size):
    print("[+] Setting up emulation environment..")
    mu = Uc(UC_ARCH_ARM, UC_MODE_THUMB)

    # Map the binary
    mu.mem_map(BASE, ALIGN_PAGE_UP(size))
    mu.mem_write(BASE, read_data(filename))
    #mu.mem_write(BASE, THUMB_CODE)
    print("[+] Binary mapped.")

    # Map the stack
    global STACK_ADDR
    STACK_ADDR = ALIGN_PAGE_UP(BASE + size)
    mu.mem_map(STACK_ADDR, STACK_SIZE)
    print("[+] Stack mapped.")

    # Set registers
    for r in REGS:
        mu.reg_write(r, 0)
    # -1024 so a function fetching arguments will not access unmapped memory.
    mu.reg_write(UC_ARM_REG_SP, STACK_ADDR + STACK_SIZE-1024)
    print("[+] Registers set.")

    # Register hooks and start emulation
    mu.hook_add(UC_HOOK_MEM_READ_UNMAPPED | UC_HOOK_MEM_WRITE_UNMAPPED, hook_mem_invalid)
    mu.hook_add(UC_HOOK_CODE, hook_code)
    print("[+] All set. Now running..")
    #print("FCT_START points to = {}".format(mu.mem_read(FCT_START, 50)))
    #print("FCT_END = {}".format(FCT_END))

    # Note we start at ADDRESS | 1 to indicate THUMB mode.
    mu.emu_start(FCT_START | 1, FCT_END)

if __name__ == "__main__":

    #filename = "/home/math/csaw/final/ChallengeSetF/TeensyChallengeSetF.ino.elf"
    filename = "TeensyPatch.elf"
    size = os.path.getsize(filename)

    #for i in range(8):
    #    print("\n[-] Trying VALUE = {}".format(VALUE))
    #    emulate(filename, size)

    ITER = 0
    emulate(filename, size)
