# 'Tower' Walkthrough

## Overview

This challenge hashes 13 bytes that are read from the RFID tag, starting at
offset 384. The resulting hash is compared with an hardcoded hash. We are
authenticated when the two hashes are the same.

## Solution

__Step #1:__ Loooking at the reversed function, we observe that there is no
additionnal computations, so we have to find those 13 bytes by ourselves. Using
brute force is not feasible, so we have to find another way.

__Step #2:__ There is a string encoded in base64 `"aHQ="` at the beginning of
the function which is passed to the function `String()`. When we decode it in utf-8,
we obtain the string `"ht"`. Unfortunately, when we hash it, we do not obtain
the expected hash.

__Step #3:__ Using Ghidra, we can list the cross-references to the function. The
`String()` function is used in a couple of functions in the binary. If we remove
those used in the other challenges, we are able to find 8 others pieces
of base64 strings by exploring the `.data` section.

Here is some python code to process them:

```python
clues = [ 'aHQ=', 'LmNvbS8=', 'dHBz', 'Z2VX', 'aW4=', 'VmU=', 'Oi8vcGFz', 'dGVi', 'bUpQ' ]

clues_str = [base64.b64decode(c).decode('utf-8') for c in clues]
# [+] String clues : 
# ['ht', '.com/', 'tps', 'geW', 'in', 'Ve', '://pas', 'teb', 'mJP']
```

At this point, we are able to identify the beginning of an URL. When we
concatenate items at index 0, 2 and 6, we obtain the string `"https://pas"`.

__Step #4:__ Using python we can test all the possible permutations and filter
with a regular expression :

```python
with open('results.txt', 'w') as file:
    for c in list(permutations(clues_str)):
        r = ''.join(c) if re.match(r'https://pas(?:[a-zA-Z]|[0-9]|[$-_@.&+] |[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', r):
            file.write(r+'\n')
```

By analyzing the output `results.txt`, we found something promising: `https://pastebin.com/`.

We have 6 valid links :

```
[...]
https://pastebin.com/geWVemJP
https://pastebin.com/geWmJPVe
https://pastebin.com/VegeWmJP <-- Good one
https://pastebin.com/VemJPgeW
https://pastebin.com/mJPgeWVe
https://pastebin.com/mJPVegeW
[...]
```

In the third link, there is the string `ndixlelxivnwl` which has a length of
13 characters.

__Setp #5:__ We can place those bytes in `senderE-tower.py` starting at offset 384.


## Validation

__Points:__ 100
__Hash:__ b019c48299dd33ec6fdc94da9d5ad06018549ee58f4a829a44d15e6980c22cbb
