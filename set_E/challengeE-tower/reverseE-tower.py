# -*- coding: utf-8 -*-

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes

from itertools import permutations

import base64
import re

chall_hash = [0x0]*32

def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def solve():
    """CSAW'19 ESC Challenge n°16
    Open the door tower
    """
    rfid_content = [0x0]*1024

    hardcoded_str = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c,
        0x65, 0x6e, 0x67, 0x65,
        0x20, 0x74, 0x6f, 0x77,
        0x65, 0x72, 0x20, 0x61,
        0x62, 0x63, 0X64, 0x65,
        0x66, 0x67, 0x68
    ]

    print('[+] Hardcoded string : {}'.format(bytes(hardcoded_str)))

    hardcoded_hash = [
        0x78, 0x0e, 0xeb, 0x0d,
        0xdd, 0x95, 0xe6, 0xb5,
        0x17, 0x4f, 0x80, 0xa0,
        0x20, 0x8d, 0x5a, 0x76,
        0x37, 0x01, 0x45, 0x5e,
        0x10, 0x82, 0xe7, 0x9d,
        0xe6, 0xdf, 0xd3, 0x42,
        0xe0, 0x49, 0x40, 0x54
    ]

    print('[+] Hardcoded hash : {}'.format(bytes(hardcoded_hash)))


    clues = [
        'aHQ=', 'LmNvbS8=', 'dHBz', 'Z2VX', 'aW4=', 'VmU=', 'Oi8vcGFz', 'dGVi', 'bUpQ'
    ]

    clues_str = [base64.b64decode(c).decode('utf-8') for c in clues]

    print('[+] String clues : ', clues_str)

    with open('results.txt', 'w') as file:
        for c in list(permutations(clues_str)):
            r = ''.join(c)
            if re.match(r'https://pas(?:[a-zA-Z]|[0-9]|[$-_@.&+] |[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', r):
                file.write(r+'\n')

    # By analysing results.txt
    # https://pastebin.com/VegeWmJP
    test = b'ndixlelxivnwl'

    digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
    digest.update(test)
    result_hash = list(digest.finalize())

    print('[+] Result hash : {}'.format(bytes(result_hash)))

    if bytes(result_hash) == bytes(hardcoded_hash):
        print('[!] Challenge success !')

        for i in range(0, 13):
            rfid_content[i+368+16] = test[i]
        display_rfid(rfid_content)
    else:
        print('[!] Challenge fail !')
 
    
if __name__ == '__main__':
    solve()