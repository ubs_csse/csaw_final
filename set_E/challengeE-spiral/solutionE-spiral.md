# 'Spiral' Walkthrough

## Overview

This challenge is similar to the `challenge 10 (break)`, it uses logical XOR, SHIFT and ADD operators in order to implement an Add-Rotate-Xor (ARX) block cipher.

## Solution

__Step #1:__

First things first. We take a few seconds to re-structure the decompiled code and rename variables. The clue encoded in Base64 tell us that we have to deal with some common bitwise operators.

__Step #2:__

We know that 4 bytes are taken from our RFID tag, those are `RFID[381]`,  `RFID[382]`,  `RFID[383]` and  `RFID[384]`. 

Those bytes are taken as entry and are mixed to produce 2 variables called `c1` and `c2`.

```txt
       Plain Text                          Cipher
                         ___________
                        |			|
    {RFID[381:384]}---->|	 ARX	|---->{c1, c2}
                        |___________|
```

The values of  `c1` and `c2` are compared with fixed values, if the following conditional statement passes then we solve the challenge.

```c
if ((c2 == 0xa29f) &&((ushort)(c2 ^ ((ushort)((int)(uint)c1 >> 0xd) | c1 << 3)) == 0xd481)) 	{...}
```

__Step #3:__

Our approach here is to implement our own version and to test the 4,294,967,296 possible combinations. Performance will be required, so we will use the `C` language to be as fast as possible and rely on OpenMP advantages to distribute our process over each logical cores our laptop disposes.

We have to make sure that RFID values are 1 byte length and output values are 2 bytes length. To avoid mistakes, we will have to cast our left shift operator.

```c
#pragma omp parallel for private(i, j, k, l) schedule(static)
for (i = 0; i < 256; i++) {
	for (j = 0; j < 256; j++) {
		for (k = 0; k < 256; k++) {
			for (l = 0; l < 256; l++) {
                unsigned short c1 = concat(k, l);
                unsigned short c2 = (c1 + (concat(i, j) << 8 | (concat(i, j)) >> 8)) ^ 0xbeef;

                c1 = c2 ^ ((c1 >> 0xd) | (unsigned short) (c1 << 3));
                c2 = (c1 + ((unsigned short)(c2 << 8) | (c2 >> 8))) ^ 0x9bb0;
                c1 = c2 ^ (((c1) >> 0xd) | (unsigned short) (c1 << 3));
                c2 = (c1 + ((unsigned short)(c2 << 8) | (c2 >> 8))) ^ 0xb499;

                if ((c2 == 0xa29f) && ((c2 ^ ((c1 >> 0xd) | (unsigned short)(c1 << 3))) == 0xd481)) 					{ ... }
            }
        }
    }
}
```

__Step #4:__

The output of our exploit gives us this result, but to be honest I always take the best coffee.

```bash
[!] Success :
    [+] RFID[397] = 0xca 
    [+] RFID[398] = 0xfe 
    [+] RFID[399] = 0xfa 
    [+] RFID[400] = 0xde 
Wall time = 6.217 sec
```

## Build Note

To build the exploit, here are the instructions:
```bash
make
./bin/solveChallengeSpiral
```

## Validation

__Points:__ 130

__Hash:__ 26ee8470c732dfc821bbe0561b446dc8086560e4e222b22e6a74e559d90a7d61
