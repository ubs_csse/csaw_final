#include <omp.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

unsigned short concat(unsigned char a, unsigned char b) {
    return b | (a << 8);
}

int main() {
	unsigned short i, j, k, l;

    double start = omp_get_wtime();

	#pragma omp parallel for private(i, j, k, l) schedule(static)
	for (i = 0; i < 256; i++) {
		for (j = 0; j < 256; j++) {
			for (k = 0; k < 256; k++) {
				for (l = 0; l < 256; l++) {
                    unsigned short c1 = concat(k, l);
                    unsigned short c2 = (c1 + (concat(i, j) << 8 | (concat(i, j)) >> 8)) ^ 0xbeef;

                    c1 = c2 ^ ((c1 >> 0xd) | (unsigned short) (c1 << 3));
                    c2 = (c1 + ((unsigned short)(c2 << 8) | (c2 >> 8))) ^ 0x9bb0;
                    c1 = c2 ^ (((c1) >> 0xd) | (unsigned short) (c1 << 3));
                    c2 = (c1 + ((unsigned short)(c2 << 8) | (c2 >> 8))) ^ 0xb499;
                    
                    if ((c2 == 0xa29f) && ((c2 ^ ((c1 >> 0xd) | (unsigned short)(c1 << 3))) == 0xd481)) {
                        printf("[!] Success :\n");
						printf("    [+] RFID[397] = 0x%02x \n", i);
						printf("    [+] RFID[398] = 0x%02x \n", j);
						printf("    [+] RFID[399] = 0x%02x \n", k);
						printf("    [+] RFID[400] = 0x%02x \n", l);
                    }
				}
			}
		}
	}

    double end = omp_get_wtime();

	fprintf(stdout, "Wall time = %.4g sec\n", end - start);

    return 0;
}
