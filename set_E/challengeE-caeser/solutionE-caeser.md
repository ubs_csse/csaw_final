# 'Caeser' Walkthrough

## Overview

This challenge reads 4 bytes from the RFID tag, 2 of them are used to compute the value of the first argument of the function named `poly()`. The last 2 bytes are computed and compared to the result of the `poly()` function.

Moreover, the button value from the packet structure is computed and used as the third argument of the `poly()` function. The button value contained in the packet correspond to the hex value of __A__ and __B__ displayed by the RFID reader, which we can change with the two buttons in the middle.

For example, if we have `A: 2   B: 4` the hex value will be __0x24__ or __36__ in decimal.

A hint is also given "What is Tungsten-A?". We only found that it is a chemical element with the symbol W and atomic number 74. We must be wrong here, because it did not help us to solve this challenge. 

## Solution

__Step #1:__ In the challenge function we can observe three calls to the function `poly()`. The first two calls return a value which is compared to hardcoded integers. The last one is compared to a computed value that we can determine by setting the correct values in the RFID tag. If the three conditions are not completed the challenge fails.

__Step #2:__ We want to identify the `poly()` function, here is the C representation of the function:

```C
unsigned int poly(unsigned int k, unsigned int *X, unsigned int Y, int len) {
    int length = len;
    unsigned int value = *X;
    int index = 0;

    while (1 < length) {
        index++;
        value = (((k * value) % 0xfff1) + X[index]) % 0xfff1;
        length--;
    }

    return (Y + ((k * value) % 0xfff1)) % 0xfff1;
}
```
__k:__  This argument correspond to the computed value `p.RFID[417]*256 + p.RFID[416]`.
__*X:__ Pointer to the adresss of a value from an array of unsigned int which is already initialized.

```C
    unsigned int X[15] = {
        0xd414, 0x9784, 0xd2b7, 0x9690, 0xf25a,
        0xe2a4, 0xbf29, 0xf35c, 0xdce3, 0xd772,
        0x3b0d, 0xd056, 0x8bf9, 0xc111, 0x8291
    };
```
__Y:__ This argument correspond to the computed value `p.buttons * 257`.
__len:__ Each call of the function uses the value 5 for this argument.

__Step #3:__ We know that we need to find three values that are satisfying the folllowing conditions:

```C
unsigned int button = p.buttons * 257;
unsigned int k = p.RFID[417]*256 + p.RFID[416];

if (poly(k, X+10, button, 5) != 0xc428) {
    // fail
}

if (poly(k, X+5, button, 5) != 0x1af6)) {
    // fail
}

if (poly(k, X, button, 5) != p.RFID[419]*256 + p.RFID[418]) {
    // fail
}
```

We observe that the value of `k` and `button` are the same in each call of `poly()`. The RFID values at byte 419 and 418 can be determined easily with the result of the last call to `poly()`.

__Step #4__ To solve this challenge we have to write a solver which is able to find the value `k` and `button` where the first two conditions will be valid.

Here is the way to solve it with OpenMP:

```C
unsigned int k;
unsigned int b;
unsigned int k_range = (256*256)+256;

unsigned int X[15] = {
    0xd414, 0x9784, 0xd2b7, 0x9690, 0xf25a,
    0xe2a4, 0xbf29, 0xf35c, 0xdce3, 0xd772,
    0x3b0d, 0xd056, 0x8bf9, 0xc111, 0x8291
};

#pragma omp parallel for private(k, b) shared(X)
for (b = 0; b < 256; b++) {
    unsigned int button = b * 257;
    for (k = 0; k < k_range; k++) {
        if ((poly(k, X+10, button, 5) == 0xc428) && (poly(k, X+5, button, 5) == 0x1af6)) {
            unsigned int out = poly(k, X, button, 5);
            printf("[!] Success k=%u button=%u:\n", k, button);
            printf("    [+] Button    = 0x%02x\n", b);
            printf("    [+] RFID[416] = 0x%02x\n", k % 256);
            printf("    [+] RFID[417] = 0x%02x\n", k / 256);
            printf("    [+] RFID[418] = 0x%02x\n", out % 256);
            printf("    [+] RFID[419] = 0x%02x\n", out / 256);

            break;
        }
    }
}
```

Ouput from `bin/solveChallengeCaeser`:

```bash
[!] Success k=43506 button=43947:
    [+] Button    = 0xab
    [+] RFID[416] = 0xf2
    [+] RFID[417] = 0xa9
    [+] RFID[418] = 0x42
    [+] RFID[419] = 0x61
Wall time = 0.125 sec
```

__Step #5__ Now we can fill the values in `sender14.py`. Finally, after programming the RFID tag, we have to set `A: 10` (0xA) and `B: 11` (0xB). Then we can submit our tag to valid the challenge.

## Build Note

To build the exploit, here are the instructions:
```bash
make
./bin/solveChallengeCaeser
```

## Validation

__Points:__ 150
__Hash:__ 551b5cff372d310b57d39b616400461be0a1450c519a2a542f33a7af0dd565f3
