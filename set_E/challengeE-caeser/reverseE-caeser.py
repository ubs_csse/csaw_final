# -*- coding: utf-8 -*-

import base64

chall_hash = [0x0]*32

def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def poly(k, X, i, Y, l):
    """
    k: rfid_read_value
    X: array of int
    i: index array
    Y: button
    l: length
    """
    index = i
    value = X[i]
    length = l
    while (1 < length):
        index += 1
        value = (((k * value) % 0xfff1) + X[index]) % 0xfff1 #65521
        length -= 1

    result = (Y + (k * value) % 0xfff1) % 0xfff1

    return result


def solve():
    """CSAW'19 ESC Challenge n°14
    Open the door caeser
    """
    pbutton = 0
    rfid_content = [0x0]*1024

    hardcoded_str = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c,
        0x65, 0x6e, 0x67, 0x65,
        0x20, 0x63, 0x61, 0x65,
        0x73, 0x65, 0x72, 0x20,
        0x61, 0x62, 0x63, 0X64,
        0x65, 0x66, 0x67
    ]

    print('[+] Hardcoded string : {}'.format(bytes(hardcoded_str)))

    clue = 'V2hhdCBpcyBUdW5nc3Rlbi1BPw=='

    print('[+] Clue : {}'.format(base64.b64decode(clue)))
    # Tungsten-A : Chemical element with the symbol W and atomic number 74

    # Use values out from the OpenMP program
    button = 0xab
    rfid_content[416] = 0xf2
    rfid_content[417] = 0xa9
    rfid_content[418] = 0x42
    rfid_content[419] = 0x61

    display_rfid(rfid_content)
    reverse(rfid_content, button)
    

def reverse(rfid_content, pbutton):
    """Reversed challenge_12 function
    """
    hardcoded_str = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c,
        0x65, 0x6e, 0x67, 0x65,
        0x20, 0x63, 0x61, 0x65,
        0x73, 0x65, 0x72, 0x20,
        0x61, 0x62, 0x63, 0X64,
        0x65, 0x66, 0x67
    ]

    button = pbutton * 257

    k = rfid_content[401+16]*256 + rfid_content[400+16]

    X = [
        0xd414, 0x9784, 0xd2b7, 0x9690, 0xf25a,
        0xe2a4, 0xbf29, 0xf35c, 0xdce3, 0xd772,
        0x3b0d, 0xd056, 0x8bf9, 0xc111, 0x8291
    ]

    fail = False

    poly_result = poly(k, X, 10, button, 5)
    if (poly_result != 0xc428):
        for i in range(0, 31):
            chall_hash[i] = ord('d')
        fail = True
        print('Fail 1')

    poly_result = poly(k, X, 5, button, 5)
    if (poly_result != 0x1af6):
        for i in range(0, 31):
            chall_hash[i] = ord('d')
        fail = True
        print('Fail 2')

    button = poly(k, X, 0, button, 5)
    if (button != rfid_content[403+16]*256 + rfid_content[402+16]):
        for i in range(0, 31):
            chall_hash[i] = ord('d')
        fail = True
        print('Fail 3')

    if (not fail):
        for i in range(0, 31):
            chall_hash[i] = hardcoded_str[i]

    chall_hash[31] = ord('\r')
    print('[!] Flag :', bytes(chall_hash))
 
    
if __name__ == '__main__':
    solve()