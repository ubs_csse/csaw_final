#include <omp.h>
#include <stdio.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

/**
 * @brief Poly function
 * 
 * @param k value read from RFID
 * @param X pointer on a value of X array
 * @param Y value of the button
 * @param len length
 * @return unsigned int 
 */
unsigned int poly(unsigned int k, unsigned int *X, unsigned int Y, int len) {
	int length = len;
	unsigned int value = *X;
	int index = 0;

	while (1 < length) {
		index++;
		value = (((k * value) % 0xfff1) + X[index]) % 0xfff1;
		length--;
	}

	return (Y + ((k * value) % 0xfff1)) % 0xfff1;
}


int main(int argc, char *argv[]) {
	unsigned int k;
	unsigned int b;
	unsigned int k_range = (256*256)+256;

	unsigned int X[15] = {
		0xd414, 0x9784, 0xd2b7, 0x9690, 0xf25a,
        0xe2a4, 0xbf29, 0xf35c, 0xdce3, 0xd772,
        0x3b0d, 0xd056, 0x8bf9, 0xc111, 0x8291
	};

	double start = omp_get_wtime();

	#pragma omp parallel for private(k, b) shared(X)
	for (b = 0; b < 256; b++) {
		unsigned int button = b * 257;
		for (k = 0; k < k_range; k++) {
			if ((poly(k, X+10, button, 5) == 0xc428) && (poly(k, X+5, button, 5) == 0x1af6)) {
				unsigned int out = poly(k, X, button, 5);
				printf("[!] Success k=%u button=%u:\n", k, button);
				printf("    [+] Button    = 0x%02x\n", b);
				printf("    [+] RFID[416] = 0x%02x\n", k % 256);
				printf("    [+] RFID[417] = 0x%02x\n", k / 256);
				printf("    [+] RFID[418] = 0x%02x\n", out % 256);
				printf("    [+] RFID[419] = 0x%02x\n", out / 256);

				break;
			}
		}
	}

	double end = omp_get_wtime();

	fprintf(stdout, "Wall time = %.4g sec\n", end - start);

    return 0;
}
