# 'Steel' Walkthrough

## Overview

This challenge is very similar to challenge_6 ('code'), but this time the hash
function is executed 9 times in a row. The other difference is that the data
used to compute the hash is not hardcoded anymore, it is a single byte, 
resulting of mathematical operations on 3 bytes read from the RFID tag.

## Solution

__Step #1:__

First, we need to find the byte value (let's call it `X`) that produces the hash
tested at the end so we can complete our equation. That is done by instrumenting
the binary at 2 key points:
- the first at `0x1762`, to inject the value to test in lieu of the one computed
    from the tag (256 possibilities)
- the second at `0x189a` to read the computed hash

`X = 14` produces the desired hash value.

__Step #2:__

We know that `X` is produced by the equation below.

```bash
X = p.RFID[0x191] - p.RFID[0x193] * p.RFID[0x192]
```

From there we can easily deduce a solution. There are many possibilities, one of
them is: 

```bash
p.RFID[0x191] = 14
p.RFID[0x193] = 0
p.RFID[0x192] = 0
```

## Validation

__Points:__ 100
__Hash:__ 5921d2ca353338c5f04c92205dc8f8bc8734f092a9e63e5f02ec106f7a7d99b4
