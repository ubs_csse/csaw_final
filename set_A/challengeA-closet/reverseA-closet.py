# -*- coding: utf-8 -*-

chall_hash = [0x00]*32
chall_ready = False


def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def solve():
    """CSAW'19 ESC Challenge n°1
    Open the door closet
    """
    rfid_content = [0x0]*1024

    print('[*] Resolving the first 12 values...')
    values = [i+12 for i in range(0, 12)]

    print('[+] Found values : {}'.format([hex(i) for i in values]).replace("'", ""))

    hardcoded_string = list(bytes('ESC19-rocks!', 'utf-8'))
    print('[+] Hardcoded string : {}'.format([hex(i) for i in hardcoded_string]).replace("'", ""))

    print('[*] Placing values in RFID content...')
    # i < 112
    rfid_content[92:104] = values
    rfid_content[104:112] = hardcoded_string[0:8]
    # i > 127 and i < 132
    rfid_content[128:132] = hardcoded_string[8:12]

    display_rfid(rfid_content)
    reverse(rfid_content)


def reverse(rfid_content):
    """Reversed challenge_1 function
    """
    rfid_read_values = [0x00]*132

    # b'ESC19-rocks!'
    hardcoded_string = [
        0x45, 0x53, 0X43, 0x31,
        0x39, 0x2d, 0x72, 0x6f,
        0X63, 0x6b, 0x73, 0x21 
    ] 
        
    print('[-] Loop 1 : Reading 24 bytes from RFID [92..111] and [128..131]...')
    for i in range(92, 132):
        if (i < 112):
            rfid_read_values[i+16] = rfid_content[i]
            print('Serial if 112 < i: i=%i, RFID_index=%i' % ((i - 92), i))
        elif (i > 127):
            rfid_read_values[i] =  rfid_content[i]
            print('Serial if 127 < i: i=%i, RFID_index=%i' % ((i - 108), i))

    print('[!] Output rfid_read_values : {}'.format(bytes(rfid_read_values)))

    print('[-] Verifying read values...')
    is_valid = True
    for i in range(0, 12):
        if(hardcoded_string[i] != rfid_read_values[rfid_read_values[i+108]+108]):
            is_valid = False
    
    # b'solved challenge closet abcdefg '
    rfid_read_values[72:104] = [      
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c,
        0x65, 0x6e, 0x67, 0x65,
        0x20, 0x63, 0x6c, 0x6f, 
        0x73, 0x65, 0x74, 0x20,
        0x61, 0x62, 0x63, 0x64,
        0x65, 0x66, 0x67, 0x20  
    ]

    print("[+] Apply b'solved challenge closet abcdefg': {}".format(bytes(rfid_read_values)))

    if (is_valid == True):
        print('[!] Success')
        for i in range(0, 31):
            chall_hash[i] = rfid_read_values[i+72]
    else:
        print('[!] Fail')
        for i in range(0, 31):
            chall_hash[i] = ord('\xff')

    chall_hash[31] = ord('\x01')
    chall_ready = True

    print('[!] Flag :', bytes(chall_hash))


if __name__ == '__main__':
    solve()