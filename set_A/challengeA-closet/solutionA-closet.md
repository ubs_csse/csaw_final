# 'Closet' Walkthrough

## Overview

The Closet challenge reads 24 bytes from the RFID tag, from byte 92 to byte 111 and from 128 to 131. Each character in the string `"ESC19-rocks!"` is compared to the RFID values read at an offset defined by the first 12 bytes read. It means that the next 12 bytes correspond to the string `"ESC19-rocks!"`.

## Solution

__Step #1:__ We can inspect the binary using Ghidra, a reverse engineering tool. We can see the hardcoded string `"ESC19-rocks!"`.

__Step #2:__ The first loop initializes a 132 byte array with values read from the RFID tag. The values read are only those from offset 92 to 111 and from offset 128 to 131. The resulting array is populated from offset 108 to 131. A python representation of this loop looks like this:

```python
for i in range(92, 132):
    if (i < 112):
        rfid_read_values[i+16] = rfid_content[i]
    elif (i > 127):
        rfid_read_values[i] = rfid_content[i]    
```

__Step #3:__ In a second loop, we also observe that each character of the hardcoded string is compared to a value from the previous resulting array at offset `rfid_read_values[i+108]+108`. If the comparison fails, a boolean is set to false, which means that the authentication will fail. Here is the python equivalent:

```python
is_valid = True
for i in range(0, 12):
    if(hardcoded_string[i] != rfid_read_values[rfid_read_values[i+108]+108]):
        is_valid = False
```

Thus, to solve this challenge we need to find the 12 values that compute the offsets of each byte stored in the last 12 bytes of the `rfid_read_values` array. Those bytes must correspond to `"ESC19-rocks!"` string.

Here is a way to recover values in python:

```python
values = [i+12 for i in range(0, 12)]
# Output hex : [0xc, 0xd, 0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17]
# Output dec : [ 12,  13,  14,  15,   16,   17,   18,   19,   20,   21,   22,   23]
```

> We add 12 in order not to access the first 12 bytes. 

String to bytes conversion:

```python
hardcoded_string = list(bytes('ESC19-rocks!', 'utf-8'))
# Output hex : [0x45, 0x53, 0x43, 0x31, 0x39, 0x2d, 0x72, 0x6f, 0x63, 0x6b, 0x73, 0x21]
```

__Step #4:__ Finally, we know that the first loop starts reading at offset `i = 92`, so we place the resolved values from offset 92 to 103.

By deduction, the next 12 bytes will be the string `"ESC19-rocks!"`. We place it in the RFID card from offset 104 to 111 and from 128 to 131. 


__Step #5:__ Moving back to the first loop, we assumed that the first condition initilizes an array using a statement like `rfid_read_values[i+16] = rfid_content[i]` with `i = 92` while `i < 112`. The second condition fill the array using the statement `rfid_read_values[i] = rfid_content[i]` while `i > 127` and `i < 132`.

We can affect automate the placement of the bytes in `sender.py` with:

```python
# i < 112
rfid_content[92:104] = values
rfid_content[104:112] = hardcoded_string[0:8]
# i > 127 and i < 132
rfid_content[128:132] = hardcoded_string[8:12]
```

Finally, this solution can be validated with `reverseA-closet.py` and then with `senderA-closet.py`.

## Validation

__Points :__ 100

__Hash:__ 8425ad5e0454e8f2398aa8a2b4a361e5670339dad91b5d81aef88fd940d7bac9
