# 'Dance' Walkthrough

## Overview

The solution to this challenge was provided as an example by the organizers to be sure that all the teams knew what to expect and were able to check that the hardware is correctly running. We found the solution to this challenge before the release of the solution, which helped us to confirm that we were doing things right.

## Solution

A full description is provided by the competion organizers (CO) here :
https://github.com/TrustworthyComputing/csaw_esc_2019/tree/master/Challenge%20Releases

We had already solved this challenge before the CO submission. Here is our Python script which describe it : `reverseA-stairs.py`

## Validation

__Points:__ 50
__Hash:__ 396f4b1cdf1cc2e7680f2a8716a18c887cd489e12232e75b6810e9d5e91426c7