# -*- coding: utf-8 -*-

chall_hash = [0x0]*32
chall_ready = False

def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def solve():
    """ESC Challenge n°3
    Open the door cafe
    """
    rfid_content = [0x0]*1024

    array_1 = [0x0]*12

    # b'ESC19-rocks!'
    array_2 = [
        0x45, 0x53, 0x43, 0x31,
        0x39, 0x2d, 0x72, 0x6f,
        0x63, 0x6b, 0x73, 0x21
    ]

    for i in range(0, 12):
        rfid_content[i+64] = array_2[i] ^ 0xf

    display_rfid(rfid_content)
    reverse(rfid_content)


def reverse(rfid_content):
    """Reverse of the function 'challenge_3()'
    """
    array_1 = [0x0]*12
    array_2 = list(b'ESC19-rocks!')
    
    is_valid = True

    for i in range(0, 12):
        array_1[i] = rfid_content[i+64]
        array_2[i] = array_2[i] ^ 0xf
        if array_2[i] != array_1[i]:
            is_valid = False

    harcoded_str = [
        0x73, 0x6f, 0x6c, 0x76,
        0x65, 0x64, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c,
        0x65, 0x6e, 0x67, 0x65,
        0x20, 0x73, 0x74, 0x61,
        0x69, 0x72, 0x73, 0x20,
        0x61, 0x62, 0x63, 0x64,
        0x65, 0x66, 0x67, 0x20
    ]

    if is_valid:
        for i in range(0, 31):
            chall_hash[i] = harcoded_str[i]
    else:
        for i in range(0, 31):
            chall_hash[i] = 0xff
    
    chall_hash[31] = 0x3

    print('[!] Flag :', bytes(chall_hash))


if __name__ == '__main__':
    solve()