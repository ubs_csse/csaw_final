﻿# 'Cafe' Walkthrough

## Overview

In this challenge, some computations are done based on 14 bytes of the RFID tag. Two independent loops of six computations each are executed. In each iteration it is possible to control whether an addition or a subtraction is performed via two bytes of the tag (one byte used per loop).

The goal is to restore the original passphrase `"solved challenge cafe abcdefghi"` from the string `"ËÄ¦r challenge °åZÇ*4bcdefghi h4x0R2 dr00L"`.

## Solution

__Step #1:__ The string we are looking for.

Using a reverse engineering tool (Ghidra) we can inspect the binary. We can see the following string: `ËÄ¦r challenge °åZÇ*4bcdefghi h4x0R2 dr00L`

After verification with command `strings AVRChallengeSetA.ino.elf | grep solved` on the AVR binary, it seems that the above string should match the following one in order to validate the challenge: `solved challenge cafe abcdefghi`.

__Step #2:__ First approach.

After analysis of the challenge function with Ghidra, we observed two loops of six iterations each. They respectively correspond to the two parts of the strings that are not correct.
These two loops take values from the RFID tag and perform some operations in order to retrieve the target string.

In each loop, we define a wanted value. A XOR operation is performed between two elements of an array and the result is stored in the actual element of the array.
After that, a value located in RFID[90] for the first loop, and in RFID[91] for the second, are used to determine if either a subtraction or an addition has to be performed on the array with another value from the tag as the second term.

After some code refactoring, here is what Ghidra gives us:

*Beware to add 16 to RFID indexes => RFID[x+16]. There is a difference of
16 between the offset from Ghidra and the actual offset of the tag.
(All offsets without an '0x' are in decimal)*

```c
// first loop
i = 0;
while (i < 6) {
    array_1[i] = array_1[i + 0x20] ^ array_1[i];    // XOR operation
    if (((int)(uint)p.RFID[74] >> i & 1U) == 0) {   // Analyze of one bit of RFID[90]
        array_1[i] = array_1[i] - p.RFID[i + 0x3e]; // Subtraction with RFID[62+i]
    }
    else {
        array_1[i] = p.RFID[i + 0x3e] + array_1[i]; // Addition with RFID[62+i]
    }
    i = i + 1;
}
// second loop
j = 17;
while (j < 23) {
    printf((Print *)&Serial,"%d %d %d\n",j,j + 17,j + -11);
    array_1[j] = array_1[j + 21] ^ array_1[j]; // XOR operation
    if (((int)(uint)p.RFID[75] >> j + -0x11 & 1U) == 0) { // Analyze of one bit of RFID[91] 
        array_1[j] = array_1[j] - p.RFID[j+51]; // Subtraction with RFID[j+67]
    }
    else {
        array_1[j] = p.RFID[j+51] + array_1[j]; // Addition with RFID[j+67]     
    }
    j = j + 1;
}
```

**So the goal here is to set values to restore researched string with words `solved` and `cafe a`.**

__Step #3:__ A few computation later...

The original array, at the beginning of the challenge, is the following:

```
0                                                                                          15
0xcb, 0x83, 0xc4, 0xa6, 0x93, 0x72, 0x20, 0x63, 0x68, 0x61, 0x6c, 0x6c, 0x65, 0x6e, 0x67, 0x65,
  -     -    -      -     -     -   space   c    h      a     l     l     e    n     g     e

16                             21                                                          31
0x20, 0xb0, 0xe5, 0x5a, 0xc7, 0x2a, 0x34, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x20
space  -     -     -     -     -     -      b     c     d     e     f     g     h     i   space

32                                    38                            43
0x68, 0x34, 0x78, 0x30, 0x52, 0x32, 0x20, 0x64, 0x72, 0x30, 0x30, 0x4c,
  h     4     x     0     R     2   space  d     r     0     0     L
```

`-` values are the values to calculate in order to retrieve the target string.
From there, calculations for the first two values of the first loop are:

```
Wanted char: s ->0x73 -> 115 (in decimal)
i = 0
array[0] = array[0+32] XOR array[0]
array[0] = 0x68 XOR 0xCB
array[0] = 0xA3 -> 163
```

In order to put the character `s` in this array element, we need to find the value 115 in decimal, so to subract one unknown value (x here) to 163:

```
163 - x = 115
0xA3 - 0x30 = 0x73
```

The value to subtract is 0x30, so **array[0] = 0x30**.  
The operation to execute is a subtraction, so **the first bit of RFID[90] must be a zero: 0bxxxxxxx0**

And so on for the six iterations:

```
s -> RFID[77]=0x30 -> Subtraction
o -> RFID[78]=0x48 -> Subtraction 
l -> RFID[79]=0x50 -> Subtraction
v -> RFID[80]=0x20 -> Subtraction
e -> RFID[81]=0x5C -> Subtraction
d -> RFID[82]=0X24 -> Addition
```

Based on the previous results, RFID[90] (defining the operation to execute (+/-)) has to be set to:

```
index i   : [5, 4, 3, 2, 1, 0]
operation : [A, S, S, S, S, S]  # (A:Addition / S:Subtraction)
binary    : [1, 0, 0, 0, 0, 0]

RFID[90]= 0b100000 (= 0x20)
```

The process is exactly the same for the second loop, except that the values of the tag are in the range RFID[84]-RFID[89]. The byte defining the type of operation to perform is located at RFID[91].

```
c -> RFID[84]=0x2D -> Subtraction
a -> RFID[85]=0x20 -> Subtraction 
f -> RFID[86]=0x3E -> Addition
e -> RFID[87]=0x92 -> Subtraction
  -> RFID[88]=0x06 -> Addition
a -> RFID[89]=0X17 -> Subtraction

index j   : [5, 4, 3, 2, 1, 0]
operation : [S, A, S, A, S, S]  # (A:Addition / S:Subtraction)
binary    : [0, 1, 0, 1, 0, 0]

RFID[90] = 0b010100 # (= 0x14)
```

__Step #4:__ Calculation by hand or take a coffee during the computation?

Of course, we can calculate all values in 15-20 minutes by hand, but it is also possible to reverse this challenge automatically with a Python script (`reverseA-cafe.py`).

```python
def solve_loop(rfid_content, offset_values, offset_cmp, supposed=[], known=[]):
    """ Solve a loop from the function 'challenge_2()'
    :rfid_content (int[1024]): RFID content to put in the tag
    :offset_values (int): Starting offset of the read values from RFID in the loop
    :offset_cmp (int): Offset of the read value from RFID to branch on an ADD or SUB
    :supposed (int[n]): Supposed bytes to find
    :known (int[n]): Known bytes after XOR operation
    """
    for i in range(0, len(supposed)):
        if supposed[i] < known[i]:
            # Subtraction
            rfid_content[offset_values+i] = known[i] - supposed[i]
        elif supposed[i] > known[i]:
            # Addition
            rfid_content[offset_values+i] = supposed[i] - known[i]
            # Branch on the else statement (addition)
            rfid_content[offset_cmp] += pow(2, i)
        else:
            # Equal
            rfid_content[offset_values+i] = known[i]
```

__Step #5:__  Place the data in the tag.

**The solution for this challenge consists in storing the following hex values inside the RFID tag:**

```bash
[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x30, 0x48],   # 4 [64..79]
[0x50, 0x20, 0x5c, 0x24, 0x2d, 0x20, 0x3e, 0x92, 0x6, 0x17, 0x20, 0x14, 0, 0, 0, 0], # 5 [80..95]
```

## Validation

__Points:__ 130
__Hash:__ d05235d380e913b5625d653c555de8925f249838896651a95bb35ea4e7863a5e
