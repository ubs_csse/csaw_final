# -*- coding: utf8 -*-

import struct


chall_hash = [0x00]*32
chall_ready = False


def display_rfid(content):
    rfid = [[hex(content[j]) for j in range(i, i+16)] for i in range(0, 1024, 16)]

    imin, imax = (0, 15) 
    for i in range(len(rfid)):
        print('{} # {} [{}..{}]'.format(rfid[i], i, imin, imax).replace("'", ""))
        imin = imax+1
        imax += 16


def solve_loop(rfid_content, offset_values, offset_cmp, supposed=[], known=[]):
    """ Solve a loop from the function 'challenge_2()'
    :rfid_content (int[1024]): RFID content to put in the tag
    :offset_values (int): Starting offset of the read values from RFID in the loop
    :offset_cmp (int): Offset of the read value from RFID to branch on an ADD or SUB
    :supposed (int[n]): Supposed bytes to find
    :known (int[n]): Known bytes after XOR operation
    """
    for i in range(0, len(supposed)):
        if supposed[i] < known[i]:
            # Subtraction
            rfid_content[offset_values+i] = known[i] - supposed[i]
        elif supposed[i] > known[i]:
            # Addition
            rfid_content[offset_values+i] = supposed[i] - known[i]
            # Branch on the else statement (addition)
            rfid_content[offset_cmp] += pow(2, i)
        else:
            # Equal
            rfid_content[offset_values+i] = known[i]


def solve():
    """ESC Challenge n°2
    Open the door cafe
    """
    rfid_content = [0x00]*1024

    array_1 = [
        0xcb, 0x83, 0xc4, 0xa6, 0x93, 0x72, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c, 0x65, 0x6e, 0x67, 0x65,
        0x20, 0xb0, 0xe5, 0x5a, 0xc7, 0x2a, 0x34, 0x62,
        0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x20,
        0x68, 0x34, 0x78, 0x30, 0x52, 0x32, 0x20, 0x64,
        0x72, 0x30, 0x30, 0x4c 
    ]

    # XOR operations
    for i in range(0, 6):
        array_1[i] = array_1[i+32] ^ array_1[i]
    # Solve the first loop
    solve_loop(
        rfid_content=rfid_content,
        offset_values=78,
        offset_cmp=90,
        supposed=list(b'solved'),
        known=array_1[0:6]
    )

    # XOR operations
    for i in range(17, 23):
        array_1[i] = array_1[i+21] ^ array_1[i]
    # Solve the second loop
    solve_loop(
        rfid_content=rfid_content,
        offset_values=67+17,
        offset_cmp=91,
        supposed=list(b'cafe a'),
        known=array_1[17:23]
    )

    reverse(rfid_content)
    display_rfid(rfid_content)


def reverse(rfid_content=[0x0]*1024):
    array_1 = [
        0xcb, 0x83, 0xc4, 0xa6, 0x93, 0x72, 0x20, 0x63,
        0x68, 0x61, 0x6c, 0x6c, 0x65, 0x6e, 0x67, 0x65,
        0x20, 0xb0, 0xe5, 0x5a, 0xc7, 0x2a, 0x34, 0x62, 
        0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x20, 
        0x68, 0x34, 0x78, 0x30, 0x52, 0x32, 0x20, 0x64, 
        0x72, 0x30, 0x30, 0x4c  
    ]

    print('[-] Reading RFID tag content...')
    for i in range(0, 6):
        array_1[i] = array_1[i+32] ^ array_1[i]

        if ((rfid_content[90] >> i & 0x1) == 0):
            array_1[i] = array_1[i] - rfid_content[i+78] 
        else:
            array_1[i] = rfid_content[i+78] + array_1[i] 

    for j in range(17, 23):
        print('[+] Serial : %i %i %i' % (j, j+17, j-11))
        array_1[j] = array_1[j+21] ^ array_1[j]

        if ((rfid_content[91] >> (j - 0x11) & 0x1) == 0):
            array_1[j] = array_1[j] - rfid_content[j+67]
        else:
            array_1[j] = rfid_content[j+67] + array_1[j]

    for k in range(0, 31):
        chall_hash[k] = array_1[k]

    chall_hash[31] = 0x2
    chall_ready = True

    print('[!] Flag :', bytes(chall_hash))
    

if __name__ == '__main__':
    solve()