# 'Lounge' Walkthrough

## Overview

This challenge uses the RSA algorithm with small numbers. The strength of RSA 
resides in the difficulty of the factorization of huge numbers, so there 
is a first apparent weakness of the program. Emulation was used to brute force the 
two one-byte factors of a multiplication to not have to re-implement the 
modifications undergone by those factors before the final operation.

## Solution

__Step #1:__ The loop we want to validate is the following piece of code. It is
the decompiled code given by Ghidra and with the variables renamed manually.

```C
if (n * q == 0x18af) {
    j = 0;
    while (j < 31) {
        challHash[j] = success_msg[j + 4];
        j = j + 1;
    }
}
```

__Step #2:__

The two variables 'a' and 'b' are computed based on two bytes read from the RFID
card. We do not need to understand what the RSA is used for (i.e. what text it
encrypt/decrypt), we just need to find two bytes for which (after modification)
the multiplication equals `0x18AF`.

- Result of the first set of operations on `p.RFID[0x4C]` and `p.RFID[0x4D]` must
  equal X.
- Result of the second set of operations on `p.RFID[0x4C]` and `p.RFID[0x4D]`
  must equal Y.

With X and Y two terms of a multiplication resulting in 0x18AF.

Since the decompiled code does seem to be mis-interpreted by Ghidra, we have to
beautify the output by expliciting the differents call methods and type
variables.

Indeed, by digging into the code we notice that functions from the library `aeabi`
are not correctly defined. For each function like `__muldf3` we explicited the
registers used to pass arguments as well as registers used to return the result,
typically if we keep focusing on `__muldf3` we know that we are passing 2
8-bytes sized parameters called `a` and `b` and we expect to have an 8-byte
sized return value. 

There is one doubt to raise. Are the functions supposed to use the stack or the
registers, and if they are using registers, which ones? The answer is, those
functions use registers. Our `a` and `b` parameters are passed using `{R0, R1,
R2, R3}` and the result is returned via `{R0, R1}`.

Once we have changed all of our functions's signatures, we have a much more
digest code and we can clearly see what is happening under the hood, leading us
to re-implement the algorithm with ease using Python.

Then the idea is to test the re-implemented function with different values
mapped to `RFID[61]` and `RFID[60]`. 

```bash
[!] Starting test
[+] v1 vaut 89 et v2 vaut 71
	i vaut 163 et j vaut 145
[+] v1 vaut 89 et v2 vaut 71
	i vaut 163 et j vaut 153
[...]
[+] v1 vaut 71 et v2 vaut 89
	i vaut 232 et j vaut 112 # Value found below using Unicorn
[...]
[+] v1 vaut 71 et v2 vaut 89
	i vaut 252 et j vaut 112
[+] v1 vaut 71 et v2 vaut 89
	i vaut 252 et j vaut 120

```

__Step #3:__

The technique used above did not work because of a mis-interpretation of `a` and
`b` that were inversed. We realized that after using emulation instead.

There is only 2 bytes used here so 256*256, or 65536 possibilities. The easy
solution is to instrument the binary to run the program for different values of
bytes read from the tag and stop when we encounter the desired result.  That way
we do not have to understand and resolve all the operations done to compute `a`
and `b`. 

The emulation was done with the Unicorn Engine framework. To place our values in
memory in lieu of what is read from the tag, we need to place hooks on the
addresses where the tag content is accessed and write the value we want to the
corresponding register. More information can be found in the script
`reverseA-lounge-1.py` and in the paper (Section V. Dynamic Analysis).

It takes only a few mintues to find a solution. Because there are multiple
solutions, we quickly found the first one.

Output from `reverseA-lounge-1.py`:

```bash
[-] Trying VALUE_4C = 112 | VALUE_4D = 232
[+] Setting up emulation environment..
[+] Binary mapped.
[+] Stack mapped.
[+] Registers set.
[+] All set. Now running..
[-] Trying to read at 0x1fff8eee
b*a = 6319
[+] Solution found!
```

## Validation

__Points:__ 100
__Hash:__ 643a6fa20b171fdf3a9e7e1975ce62892fde9cecf2056a73d85fa2d0802d3000
