import math


def init_v1_v2(rfid_61, rfid_60):
    v1 = ((rfid_61 >> 7) * 0x40) + ((rfid_60 >> 4) & 1) + ((rfid_61 >> 6 & 1) * 2) + \
         ((rfid_61 >> 3 & 1) * 4) + ((rfid_61 & 1) * 8) + ((rfid_60 >> 7) * 0x10) + ((rfid_60 >> 1 & 1) * 0x20)

    v2 = (((rfid_61 >> 5) & 1) * 0x40) + ((rfid_61 >> 5) & 1) + ((rfid_60 & 1) * 2) + \
         (((rfid_61 >> 1) & 1) * 4) + (((rfid_60 >> 6) & 1) * 8) + (((rfid_60 >> 5) & 1) * 0x10) + \
         (((rfid_60 >> 2) & 1) * 0x20)

    return v1, v2


print("[!] Starting test")
rfid_61 = 0
rfid_60 = 0


for i in range(0, 257):
    for j in range(0, 257):
        r1, r2 = init_v1_v2(i, j)

        a = (j >> 3) & 1
        b = (i >> 2) & 1

        a = a * r1
        a = a - 0x3ff00000


        b = b * r2
        b = b - 0x3ff00000

        a = a * b

        loop = 1
        ok = 0
        limit_a = 0

        while loop:
            u = a > limit_a

            if u != 0:
                break

            u = 0
            v = a
            u = math.gcd(u, v)
            b = u
            u = (b == 0x3ff00000)

            if u != 0:
                break
            limit_a = limit_a + 0

        if r1*r2 == 6319:
            print(f"[!] v1 = {r1} and v2 = {r2}")
            print(f"[-] i = {hex(i)}\n[-] j = {hex(j)}")
