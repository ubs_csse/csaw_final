#!/usr/bin/env python3

import os

from unicorn import *
from unicorn.arm_const import *

BASE = 0
# Start of section .text
TEXT = 0x10000
STACK_ADDR = 0x0
STACK_SIZE = 1024*1024
PAGE_SIZE = 0x1000

# Information related to the function to emulate
#FCT_START = 0x7E4
FCT_START = 0x898 # in startChallenge()
# Not the actual end, we don't need to execute everything
#FCT_END = 0x8D2 # in startChallenge()
FCT_END = 0x872 # in fillHashChallenge()

VALUE = 0x0
ITER = 0

REGS = [ UC_ARM_REG_R0, UC_ARM_REG_R1, UC_ARM_REG_R2, UC_ARM_REG_R3,
         UC_ARM_REG_R4, UC_ARM_REG_R5, UC_ARM_REG_R6, UC_ARM_REG_R7,
         UC_ARM_REG_R8, UC_ARM_REG_R9, UC_ARM_REG_R10, UC_ARM_REG_R11,
         UC_ARM_REG_R12, UC_ARM_REG_PC, UC_ARM_REG_SP, UC_ARM_REG_LR,
         UC_ARM_REG_CPSR ]

def ALIGN_PAGE_DOWN(x):
    return x & ~(PAGE_SIZE - 1)

def ALIGN_PAGE_UP(x):
    return (x + PAGE_SIZE - 1) & ~(PAGE_SIZE-1)

def hook_mem_invalid(emu, access, addr, size, value, user_data):
    """
    Catch illegal memory operation. Allocate desired memory and continue.
    """
    if access == UC_MEM_WRITE_UNMAPPED:
        print("[-] Trying to write at {}".format(hex(addr)))
        # Trying to write to unmapped memory. Allocating some at target address
        # before resuming.
        s = ALIGN_PAGE_DOWN(addr)
        e = ALIGN_PAGE_UP(addr+size)
        emu.mem_map(s, e - s)

        # Return True to indicate we want to continue emulation
        return True
    elif access == UC_MEM_READ_UNMAPPED:
        print("[-] Trying to read at {}".format(hex(addr)))
        s = ALIGN_PAGE_DOWN(addr)
        e = ALIGN_PAGE_UP(addr+size)
        emu.mem_map(s, e - s)

        return True

def hook_code(mu, address, size, user_data):
    # DEBUG
    #if address > 0x872:
    #    print("(debug) Tracing instruction at {} (size = {})".format(hex(address), size))
    if address == 0x888:
        lr = mu.reg_read(UC_ARM_REG_LR)
        print("LR = {}".format(hex(lr)))


    global VALUE
    global ITER

    # Read variables
    if address == 0x802:
        r2 = mu.reg_read(UC_ARM_REG_R2)
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("local_14[0] = {}".format(hex(r2)))
        print("local_14[1] = {}".format(hex(r3)))
    if address == 0x804:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("local_14[0] ^ local_14[1] = {}".format(hex(r3)))

    # Check @ local_14
    if address == 0x808:
        r7 = mu.reg_read(UC_ARM_REG_R7)
        print("@local_14     = {}".format(hex(r7+0x14))) # stack + offset_local_14
        print("@local_1c (i) = {}".format(hex(r7+0x10-0x4))) # stack + offset_i
        print("@local_1d     = {}".format(hex(r7+0x10-0x5))) # stack + offset_local_ld
        #debug = mu.mem_read(0x163be0, 4)
        #print("0x163be0      = {}".format(debug))

    # Replace byte read from the tag to enter the loop and write to local_28[]
    if address == 0x828:
        #if ITER == 11:
        #    mu.reg_write(UC_ARM_REG_R1, 11)
        if ITER == 20:
            mu.reg_write(UC_ARM_REG_R1, 20)
        elif ITER == 21:
            mu.reg_write(UC_ARM_REG_R1, 32)
        ITER += 1

    # Force CMP not to jump (and iterate 48 times)
    #if address == 0x842:
    #    if ITER < 0x30:
    #        mu.reg_write(UC_ARM_REG_R3, 1)
    #        ITER += 1
    # Force final CMP to success loop
    #if address == 0x870:
    #    mu.reg_write(UC_ARM_REG_R2, 0)
    #    mu.reg_write(UC_ARM_REG_R3, 0)

    # Replace byte read from the tag (what is written to local_28[])
    if address == 0x852:
        # Do not reset the counter (i)
        #if ITER == 13:
        #    mu.reg_write(UC_ARM_REG_R1, 12)
        # Overwrite local_ld
        #elif ITER == 12:
        #    mu.reg_write(UC_ARM_REG_R1, 0x1D)
        # Overwrite local_14 (and additionaly LR)
        if ITER == 21:
            mu.reg_write(UC_ARM_REG_R1, 0x1D)
        # Overwrite local_14 (and additionaly LR)
        elif ITER == 22:
            mu.reg_write(UC_ARM_REG_R1, 0x7)

    # Get value of r.buttons
    if address == 0x86A:
        r3 = mu.reg_read(UC_ARM_REG_R3)
        print("r.buttons = {}".format(hex(r3)))

    if address == 0x85a:
        if ITER == 12 or ITER == 21 or ITER == 22:
            r3 = mu.reg_read(UC_ARM_REG_R3)
            data = mu.mem_read(r3, 1)
            print("@local_28 = {} | points to {}".format(hex(r3), data))
        #ITER += 1
        #r3 = mu.reg_read(UC_ARM_REG_R3)
        #print("@local_28 = {} | points to {}".format(hex(r3), data))

    # Check values at final CMP
    if address == 0x866:
        r2 = mu.reg_read(UC_ARM_REG_R2)
        print("local_14 = {}".format(hex(r2)))
    if address == 0x870:
        r2 = mu.reg_read(UC_ARM_REG_R2)
        print("local_ld = {}".format(hex(r2)))

    if address == 0x876:
        print("[+] Entering success loop!")

def read_data(filename):
    with open(filename, "rb") as f:
        # Copy only from TEXT to synchronize instruction addresses with the
        # disassembly since .text section as a decalage of TEXT.
        f.seek(TEXT, 0)
        data = f.read()
        f.close()
    return data

def emulate(filename, size):
    print("[+] Setting up emulation environment..")
    mu = Uc(UC_ARCH_ARM, UC_MODE_THUMB)

    # Map the binary
    mu.mem_map(BASE, ALIGN_PAGE_UP(size))
    mu.mem_write(BASE, read_data(filename))
    #mu.mem_write(BASE, THUMB_CODE)
    print("[+] Binary mapped.")

    # Map the stack
    global STACK_ADDR
    STACK_ADDR = ALIGN_PAGE_UP(BASE + size)
    mu.mem_map(STACK_ADDR, STACK_SIZE)
    print("[+] Stack mapped.")

    # Set registers
    for r in REGS:
        mu.reg_write(r, 0)
    # -1024 so a function fetching arguments will not access unmapped memory.
    mu.reg_write(UC_ARM_REG_SP, STACK_ADDR + STACK_SIZE-1024)
    print("[+] Registers set.")

    # Register hooks and start emulation
    mu.hook_add(UC_HOOK_MEM_READ_UNMAPPED | UC_HOOK_MEM_WRITE_UNMAPPED, hook_mem_invalid)
    mu.hook_add(UC_HOOK_CODE, hook_code)
    print("[+] All set. Now running..")
    #print("FCT_START points to = {}".format(mu.mem_read(FCT_START, 50)))
    #print("FCT_END = {}".format(FCT_END))

    # Note we start at ADDRESS | 1 to indicate THUMB mode.
    mu.emu_start(FCT_START | 1, FCT_END)

if __name__ == "__main__":

    filename = "patch.elf"
    size = os.path.getsize(filename)

    emulate(filename, size)
    print(ITER)
