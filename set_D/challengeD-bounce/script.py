#!/usr/bin/env python3

i = 20
j = 21

for x in range(256):
    var1 = i & 0x80000007
    var2 = j & 0x80000007

    if ((x >> var1) & 1) != 0 and ((x >> var2) & 1) != 0:
        print("value to enter the loop : {} | offset : {}".format(x, (i>>3)+0x100) )

