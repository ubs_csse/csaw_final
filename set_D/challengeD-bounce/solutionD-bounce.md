# 'Bounce' Walkthrough

## Overview

_In the binary of the set D, this challenge is represented by the function
`challenge_8()`. In order to keep the flow with our current numerotation, we will call it
challenge 12._

In this challenge just validating the authentication check is not enough to
validate it. The `challHash` array filled in the success loop is set in another
function (`fillChallengeHash()`) which is not called after the call of
`challenge_8()`. However there is a buffer overflow situation that we can use to
force the success function to be called.


## Solution

__Step #1:__

The `local_28` variable is an array of 11 bytes. The loop of which the code below
is extracted iterates 48 times.

```c
    [...]
    uVar2 = i & 0x80000007;
    [...]

    if ( (p.RFID[(i >> 3) + 0xc0] >> uVar2 & 1U) != 0 ) {
        local_28[i] = p.RFID[i + 0xc0];
    }
    i = i + 1;
}
```

We used emulation to have better insight of what is happening at runtime, and to
get a view of the stack layout. What we want is the offset of the different
variables. We could have done this manually by looking at the ARM assembly code
but it is time consuming and error prone.

At first we did not emulate enough of the program: we did not notice at first
that `challenge_8()` was called with a parameter (0x8D1) so in the context of
the emulator all the variables were initialized at 0 and we did not understand
why the following condition was not satisfied when only 0s were passed in the
tag (not passing this test was materialized by the board looping for a certain
amount of time).

```c
if (local_1d != (p.buttons ^ (byte)local_14)) {
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
}
```

__Step #2:__

`local_ld` and `local_14` are initialized by these instructions:

```c
local_14._0_1_ = (byte)in_lr;
local_14._1_1_ = (byte)((uint)in_lr >> 8);
local_1d = local_14._1_1_ ^ (byte)local_14;
```

When we realized we needed to emulate the function call too, we obtain the
following in our stack:
```bash
local_14[0] = 0xd1               # <= operand of the final XOR
local_14[1] = 0x8                # <= value of p.buttons in the final XOR
local_14[0] ^ local_14[1] = 0xd9 # <= what goes in local_ld
@local_14  = 0x1637a4
@i         = 0x16379c
@local_1d  = 0x16379b
@local_28  = 0x163790
```

_The offsets of the instructions we instrumented are not mentionned here. You can
check `reverseD-bounce.py` to get them with additional comments._

Based on this we found that setting the B button to 8, was enough to validate
the test (i.e. not entering the while loop) because: `0xd9 == 0x8 ^ 0xd1`.

__Step #3:__

In ARM assembly, when a function is called, the instruction following the call
(in the caller not in the callee) is saved in the __LR__ register (hence the
__in_lr__ in the decompiled code) which acts as the __Saved RIP__ saved on the
stack on x86_64 processors. When a function return with the __BX LR__
instruction, the program will continue its execution at the address contained in
__LR__. What we learned from this is that the `local_14` variable points to the
value of the stack that will be pop'ed into __LR__ during the function epilogue.

So what we want now is to overwrite what is at the address of `local_14` so that
__LR__ will point to the address of `fillChallengeHash()` + 1 (0x71D). The +1
indicates that the destination instruction length is 2 bytes (THUMB mode).

The final XOR comparison become `0xd9 == p.buttons ^ 0x1d` so `p.buttons` has to be
set to 0xc4 (A=12 and B=4).

__Step #4:__

In order to overwrite the variable we need to satisfy the `if` statement below
(cf. __Step #1__).

```c
    [...]
    uVar2 = i & 0x80000007;
    [...]

    if ( (p.RFID[(i >> 3) + 0x100] >> uVar2 & 1U) != 0 ) {
        local_28[i] = p.RFID[i + 0xc0];
    }
    i = i + 1;
}
```

We want to overwrite the stack for i = 20 and i = 21. However `20 >> 3 == 21 >> 3
== 2` which means that the value at offset 258 (0x100 + 2) in the tag has to
satisfy the equation for both value of i. The following script can list all the
possible values:

```python
i = 20
j = 21

for x in range(256):
    var1 = i & 0x80000007
    var2 = j & 0x80000007

    if ((x >> var1) & 1) != 0 and ((x >> var2) & 1) != 0:
        print("value to enter the loop : {} | offset : {}".format(x, (i>>3)+0x100) )
```

To resume, here is what the tag must contain:
- p.RFID[258] = 48 (given by the script above);
- p.RFID[212] = 0x1D;
- p.RFID[213] = 0x7;

## Validation

__Points:__ 150.
__Hash:__ ef916aaaff05b978da738ca96cb3b56c8954d092e272d1438c745337e602ecc8.
